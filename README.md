# [<img src="/misc/NuGetIcon.png" width="32" height="32">](/misc/NuGetIcon.png) ProtonSynth [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) ![Nuget](https://img.shields.io/nuget/v/Skelp.ProtonSynth) ![Discord](https://img.shields.io/discord/826901818834550794) [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Skelp_protonsynth&metric=alert_status)](https://sonarcloud.io/dashboard?id=Skelp_protonsynth)

ProtonSynth is a C# based framework composed of ready-to-use synthesizer building blocks such as signal generators, envelopes and signal processors. 

The project aims to provide a high-level abstraction in order to speed up designing synthesizers.

**The framework does not manage direct audio output**. You may use the framework in conjunction with, but not limited to, libraries such as [NAudio](https://github.com/naudio/NAudio) for direct audio output or [VST.NET](https://github.com/obiwanjacobi/vst.net) to be used as a plugin in a [DAW](https://en.wikipedia.org/wiki/Digital_audio_workstation).

Starting from version `0.6.0` you may use the `Skelp.ProtonSynth.WaveBuilder` class to create a `.wav` RIFF file in memory, which you can save as a file on a drive using `System.File.WriteAllBytes`. More on that in the [examples](/EXAMPLES.md).

The project is losely adhering to conventions brought on by [StyleCop rules](https://github.com/DotNetAnalyzers/StyleCopAnalyzers).

Development takes place in the develop branch. Stable releases will be merged into the master branch starting from version 1.0.0, development will continue in the develop branch.

## Table Of Contents

1. [Installation](#installation)
2. [Getting Started](#getting-started)
3. [Join The Discord](#join-the-discord)
4. [Examples](#examples)
5. [Roadmap](#roadmap)

## Installation

Simply use the Visual Studio NuGet Package Manager and look for `ProtonSynth`.

Alternatively, you can use the Package Manager Console as such:
> `Install-Package Skelp.ProtonSynth`

If you want to use the .NET CLI, you can use the following command:
> `dotnet add package Skelp.ProtonSynth`

## Getting Started

The ProtonSynth framework is logically split into three namespaces.

1. **Signal Generators** (`Skelp.ProtonSynth.SignalGenerators`)
2. **Envelopes** (`Skelp.ProtonSynth.Envelopes`)
3. **Signal Processors** (`Skelp.ProtonSynth.SignalProcessors`)

**Signal generators** will compute a new sample based on their internal state when calling `GetSample()`. The method returns a `double` between `-1` and `+1`.

**Envelopes** produce amplitude samples when calling `GetAmplitude()` with values of type `double` in the range of `0` and `+1`.

And finally, **signal processors** take in samples ranging from `-1` to `+1` and return a processed sample with in the same range by calling `ProcessSample(double sample)`.

These components can be routed with one another in any way you like.
For instance, you can create a sinewave oscillator and an ADSR envelope modulating the oscillator output amplitude. 

```csharp
using Skelp.ProtonSynth.SignalGenerators;
using Skelp.ProtonSynth.Envelopes;
```
`[...]`
```csharp
int sampleRate = 44100;
double frequency = 440;
double maxAmplitude = 1;

SineOscillator osc = new SineOscillator(sampleRate, frequency, maxAmplitude);
LinearEnvelope ampEnvelope = new LinearEnvelope(sampleRate);

// Setting up envelope parameters
ampEnvelope.AttackTime = TimeSpan.FromSeconds(0.1);
ampEnvelope.DecayTime = TimeSpan.FromSeconds(1);
ampEnvelope.SustainAmplitude = 0.5;
ampEnvelope.ReleaseTime = TimeSpan.FromSeconds(0.1);

// Starts the envelope
ampEnvelope.TriggerOn();

// Synthesize a single sample (1 out of 44100 per second) and modulate its amplitude
double nextSample = osc.GetSample() * ampEnvelope.GetAmplitude();
```

## Join The Discord

If you need any support, have some issues, suggestions or questions for or about the project, feel free to join our Discord.

[![Join our Discord server!](https://invidget.switchblade.xyz/JwAJh5hmvy)](https://discord.gg/JwAJh5hmvy)

## Examples

If you want to find out how to use the framework, please refer to the [auto-generated documentation](ProtonSynth/Documentation/index.md) and check out some [examples](/EXAMPLES.md).

## Roadmap

A rough roadmap can be found below.  
The bullet points are subject to change and feature suggestions may be added to the roadmap.

The following features will be implemented before the release of version `1.0.0`:

* Chorus effect
* Reverb effect
* The rest of the missing unit tests
* Provide example-projects for use with [NAudio](https://github.com/naudio/NAudio), [VST.NET](https://github.com/obiwanjacobi/vst.net) and maybe even Unity
* Remodelling the PolyphonyManager class (=> interface, IVoice)
* Provide a couple of simple, ready-built synthesizer models

After version `1.0.0`:
* Focusing on improving performance by using performance tests
* Develop interfaces & classes to help build:
    * FM synthesizers
    * Modular synthesizers
    * Additive synthesizers
* Implement MIDI parser
