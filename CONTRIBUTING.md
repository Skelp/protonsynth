# ProtonSynth: Code Contributions

Anyone is welcome to resolve existing issues, enhance existing code or add new code.  
If you do decide to help the project out, please make sure your code changes are in accordance with the [Developer Certificate Of Origin](DCO).

Any code submitted may be subject to change. To reduce said changes to a minimum, please make sure your code adheres to most [StyleCop rules](https://github.com/DotNetAnalyzers/StyleCopAnalyzers).  
Some rules, such as [SA1101](https://github.com/DotNetAnalyzers/StyleCopAnalyzers/blob/master/documentation/SA1101.md) may be ignored.  
Testing your own code also minimzes the need for changes.

In general, just by looking at existing code from this project and taking note on how the code was written should be enough to write code that will be accepted without many changes, if any at all.

Any change shall be commited to the `develop` branch. Starting from version `1.0.0`, the project will enforce the use of the [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

You are very welcome to join our Discord server to discuss features, issues and unresolved questions.

[![Join our Discord server!](https://invidget.switchblade.xyz/JwAJh5hmvy)](https://discord.gg/JwAJh5hmvy)
