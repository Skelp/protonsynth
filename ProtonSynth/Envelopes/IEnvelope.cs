﻿using Skelp.ProtonSynth.SignalGenerators;
using System;

namespace Skelp.ProtonSynth.Envelopes
{
    /// <summary>
    /// An interface that describes any signal source traditionally used as a multiplier for the maximum amplitude of any <see cref="ISignalGenerator"/>.
    /// </summary>
    public interface IEnvelope
    {
        /// <summary>
        /// Gets or sets the maximum value of the <see cref="IEnvelope"/>.
        /// </summary>
        UnitInterval AmplitudeMultiplier { get; set; }

        /// <summary>
        /// Gets or sets the time spent in the attack phase of the <see cref="IEnvelope"/>.
        /// </summary>
        TimeSpan AttackTime { get; set; }

        /// <summary>
        /// Gets or sets the time spent in the decay phase of the <see cref="IEnvelope"/>.
        /// </summary>
        TimeSpan DecayTime { get; set; }

        /// <summary>
        /// Gets or sets the time spent in the release phase of the <see cref="IEnvelope"/>.
        /// </summary>
        TimeSpan ReleaseTime { get; set; }

        /// <summary>
        /// Gets or sets the samplerate of the <see cref="IEnvelope"/>.
        /// </summary>
        int SampleRate { get; }

        /// <summary>
        /// Gets or sets the amplitude the <see cref="IEnvelope"/> starts with once when triggered on.
        /// </summary>
        UnitInterval StartAmplitude { get; set; }

        /// <summary>
        /// Gets or sets the amplitude the <see cref="IEnvelope"/> maintains when <see cref="DecayTime"/> is over.
        /// </summary>
        UnitInterval SustainAmplitude { get; set; }

        /// <summary>
        /// Gets the next sample according to the inner state of the <see cref="IEnvelope"/>.
        /// </summary>
        /// <returns>A <see cref="UnitInterval"/>.</returns>
        UnitInterval GetAmplitude();

        /// <summary>
        /// Triggers the attack phase of the <see cref="IEnvelope"/>.
        /// </summary>
        void TriggerOn();

        /// <summary>
        /// Triggers the release phase of the <see cref="IEnvelope"/>.
        /// </summary>
        void TriggerOff();
    }
}