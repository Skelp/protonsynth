﻿using System;

namespace Skelp.ProtonSynth.Envelopes
{
    /// <summary>
    /// Implementation of a linear ADSR <see cref="IEnvelope"/>.
    /// </summary>
    public class LinearEnvelope : Envelope
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LinearEnvelope"/> class.
        /// </summary>
        /// <param name="sampleRate">The samplerate for the new <see cref="LinearEnvelope"/>.</param>
        public LinearEnvelope(int sampleRate = 44100)
            : base(sampleRate)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinearEnvelope"/> class.
        /// </summary>
        /// <param name="sampleRate">The samplerate for the new <see cref="LinearEnvelope"/>.</param>
        /// <param name="attackTime">Relative time spent in the attack phase.</param>
        /// <param name="decayTime">Relative time spent in the decay phase.</param>
        /// <param name="sustainAmplitude">The amplitude being held when <paramref name="decayTime"/> is over.</param>
        /// <param name="releaseTime">Relative time spent in the release phase.</param>
        public LinearEnvelope(TimeSpan attackTime, TimeSpan decayTime, UnitInterval sustainAmplitude, TimeSpan releaseTime, int sampleRate = 44100)
            : base(attackTime, decayTime, sustainAmplitude, releaseTime, sampleRate)
        {
        }

        /// <inheritdoc/>
        public override UnitInterval GetAmplitude()
        {
            double amplitude = 0;

            if (EnvelopeIsTriggeredOn)
            {
                // Attack
                if (SampleCounter < AttackTimeInSamples)
                {
                    var delta = AttackTimeInSamples - (AttackTimeInSamples - SampleCounter);
                    amplitude = ValueHandler.ChangeRange(delta, 0, AttackTimeInSamples, StartAmplitude, 1);
                    SampleCounter++;
                }
                // Decay
                else if (SampleCounter < (AttackTimeInSamples + DecayTimeInSamples))
                {
                    var sampleDelta = SampleCounter - AttackTimeInSamples;
                    var currentUnnormalizedValue = DecayTimeInSamples - sampleDelta;
                    var normalized = ValueHandler.ChangeRange(currentUnnormalizedValue, 0, DecayTimeInSamples, SustainAmplitude, 1);
                    amplitude = normalized;
                    SampleCounter++;
                }
                // Sustain
                else if(SampleCounter >= (AttackTimeInSamples + DecayTimeInSamples))
                {
                    amplitude = SustainAmplitude;
                }

                LastAmplitude = amplitude;
            }
            else
            {
                // Release
                if (SampleCounter > ReleaseTimeInSamples)
                {
                    amplitude = 0;
                }
                else
                {
                    if (ReleaseTimeInSamples != 0)
                    {
                        amplitude = LastAmplitude * (1 - ((SampleCounter + 1) / (double)ReleaseTimeInSamples));
                    }

                    SampleCounter++;
                }
            }

            return amplitude * AmplitudeMultiplier;
        }
    }
}