﻿using System;

namespace Skelp.ProtonSynth.Envelopes
{
    /// <summary>
    /// Implementation of an ADSR <see cref="IEnvelope"/> based on <see cref="LinearEnvelope"/>.
    /// </summary>
    public class PowerEnvelope : LinearEnvelope
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PowerEnvelope"/> class.
        /// </summary>
        /// <param name="sampleRate">The samplerate for the new <see cref="PowerEnvelope"/>.</param>
        public PowerEnvelope(int sampleRate = 44100)
            : base(sampleRate)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerEnvelope"/> class.
        /// </summary>
        /// <param name="sampleRate">The samplerate for the new <see cref="PowerEnvelope"/>.</param>
        /// <param name="attackTime">Relative time spent in the attack phase.</param>
        /// <param name="decayTime">Relative time spent in the decay phase.</param>
        /// <param name="sustainAmplitude">The amplitude being held when <paramref name="decayTime"/> is over.</param>
        /// <param name="releaseTime">Relative time spent in the release phase.</param>
        public PowerEnvelope(TimeSpan attackTime, TimeSpan decayTime, UnitInterval sustainAmplitude, TimeSpan releaseTime, int sampleRate = 44100)
            : base(attackTime, decayTime, sustainAmplitude, releaseTime, sampleRate)
        {
        }

        /// <inheritdoc/>
        public override UnitInterval GetAmplitude()
        {
            UnitInterval result = base.GetAmplitude();
            return result * result;
        }
    }
}