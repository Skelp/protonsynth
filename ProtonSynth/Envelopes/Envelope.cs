﻿using System;

namespace Skelp.ProtonSynth.Envelopes
{
    /// <summary>
    /// Barebone implementation of an <see cref="IEnvelope"/>.
    /// </summary>
    public abstract class Envelope : IEnvelope
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Envelope"/> class.
        /// </summary>
        /// <param name="sampleRate">The samplerate for the new <see cref="Envelope"/>.</param>
        private protected Envelope(int sampleRate = 44100)
        {
            SampleRate = sampleRate;
            this.SustainAmplitude = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Envelope"/> class.
        /// </summary>
        /// <param name="sampleRate">The samplerate for the new <see cref="Envelope"/>.</param>
        /// <param name="attackTime">Relative time spent in the attack phase.</param>
        /// <param name="decayTime">Relative time spent in the decay phase.</param>
        /// <param name="sustainAmplitude">The amplitude being held when <paramref name="decayTime"/> is over.</param>
        /// <param name="releaseTime">Relative time spent in the release phase.</param>
        private protected Envelope(TimeSpan attackTime, TimeSpan decayTime, UnitInterval sustainAmplitude, TimeSpan releaseTime, int sampleRate = 44100)
        {
            SampleRate = sampleRate;
            AttackTime = attackTime;
            DecayTime = decayTime;
            SustainAmplitude = sustainAmplitude;
            ReleaseTime = releaseTime;
        }

        /// <summary>
        /// Gets the samplerate of the <see cref="Envelope"/>.
        /// </summary>
        public int SampleRate { get; private protected set; }

        /// <inheritdoc/>
        public UnitInterval AmplitudeMultiplier { get; set; } = 1;

        /// <inheritdoc/>
        public TimeSpan AttackTime { get => GetSamplesAsTime(AttackTimeInSamples); set => AttackTimeInSamples = GetTimeInSamples(value); }

        /// <inheritdoc/>
        public TimeSpan DecayTime { get => GetSamplesAsTime(DecayTimeInSamples); set => DecayTimeInSamples = GetTimeInSamples(value); }

        /// <inheritdoc/>
        public TimeSpan ReleaseTime { get => GetSamplesAsTime(ReleaseTimeInSamples); set => ReleaseTimeInSamples = GetTimeInSamples(value); }

        /// <inheritdoc/>
        public UnitInterval StartAmplitude { get; set; }

        /// <inheritdoc/>
        public UnitInterval SustainAmplitude { get; set; }

        /// <summary>
        /// Gets or sets the most recent position within the <see cref="Envelope"/> relative to its trigger state.
        /// </summary>
        private protected int SampleCounter { get; set; }

        /// <summary>
        /// Gets or sets the value of the most recent amplitude.
        /// </summary>
        private protected double LastAmplitude { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the <see cref="Envelope"/> is triggered on.
        /// </summary>
        private protected bool EnvelopeIsTriggeredOn { get; set; }

        /// <summary>
        /// Gets or sets the attack time in samples based on <see cref="SampleRate"/>.
        /// </summary>
        private protected int AttackTimeInSamples { get; set; }

        /// <summary>
        /// Gets or sets the decay time in samples based on <see cref="SampleRate"/>.
        /// </summary>
        private protected int DecayTimeInSamples { get; set; }

        /// <summary>
        /// Gets or sets the release time in samples based on <see cref="SampleRate"/>.
        /// </summary>
        private protected int ReleaseTimeInSamples { get; set; }

        /// <inheritdoc/>
        public abstract UnitInterval GetAmplitude();

        /// <summary>
        /// Sets the samplerate of the <see cref="Envelope"/>.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate.</param>
        public void SetSampleRate(int sampleRate)
        {
            if (sampleRate < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(sampleRate));
            }

            var oldAttack = AttackTime;
            var oldDecay = DecayTime;
            var oldRelease = ReleaseTime;

            SampleRate = sampleRate;

            AttackTime = oldAttack;
            DecayTime = oldDecay;
            ReleaseTime = oldRelease;
        }

        /// <inheritdoc/>
        public void TriggerOn()
        {
            SampleCounter = 0;
            EnvelopeIsTriggeredOn = true;
        }

        /// <inheritdoc/>
        public void TriggerOff()
        {
            SampleCounter = 0;
            EnvelopeIsTriggeredOn = false;
        }

        /// <summary>
        /// Converts time to samples based on <see cref="SampleRate"/>.
        /// </summary>
        /// <param name="timeSpan">Any <see cref="TimeSpan"/>.</param>
        /// <returns>An <see cref="int"/> representing time in samples relative to <see cref="SampleRate"/>.</returns>
        private protected int GetTimeInSamples(TimeSpan timeSpan)
        {
            return (int)Math.Round(timeSpan.TotalSeconds * SampleRate);
        }

        /// <summary>
        /// Converts samples to time based on <see cref="SampleRate"/>.
        /// </summary>
        /// <param name="samples">Any time in samples.</param>
        /// <returns>A <see cref="TimeSpan"/> with a length of <paramref name="samples"/> relative to <see cref="SampleRate"/>.</returns>
        private protected TimeSpan GetSamplesAsTime(int samples)
        {
            return TimeSpan.FromSeconds(samples / (double)SampleRate);
        }
    }
}