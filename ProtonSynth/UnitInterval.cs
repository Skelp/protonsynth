﻿using System;

namespace Skelp.ProtonSynth
{
    /// <summary>
    /// Represents a real number between 0 and 1.
    /// </summary>
    public struct UnitInterval
    {
        /// <summary>
        /// Represents the maximum value a <see cref="UnitInterval"/> can hold.
        /// </summary>
        public const double MaxValue = 1;

        /// <summary>
        /// Represents the minimum value a <see cref="UnitInterval"/> can hold.
        /// </summary>
        public const double MinValue = 0;

        private double value;

        private double Value
        {
            get { return value; }
            set { this.value = value <= MaxValue && value >= MinValue ? value : throw new ArgumentOutOfRangeException(nameof(value)); }
        }

        public static implicit operator double(UnitInterval d) => d.Value;

        public static implicit operator UnitInterval(double d) => new UnitInterval() { Value = d };

        public override string ToString() => value.ToString();

        public string ToString(string format, IFormatProvider provider) => value.ToString(format, provider);

        public string ToString(string format) => value.ToString(format);

        public string ToString(IFormatProvider provider) => value.ToString(provider);
    }
}