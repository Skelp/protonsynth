#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors')
## FilterType Enum
Identifies and controls the type of [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter').  
```csharp
public enum FilterType

```
#### Fields
<a name='Skelp_ProtonSynth_SignalProcessors_FilterType_HighPass'></a>
`HighPass` 1  
Represents a highpass filter.  
  
<a name='Skelp_ProtonSynth_SignalProcessors_FilterType_LowPass'></a>
`LowPass` 0  
Represents a lowpass filter.  
  
