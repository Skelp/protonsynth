#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[WavetableOscillator](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator')
## WavetableOscillator.TablePosition Property
Gets or sets the position within the [WaveformTable](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator_WaveformTable.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator.WaveformTable') where 0 represents the beginning and 1 the end of the array.  
```csharp
public Skelp.ProtonSynth.UnitInterval TablePosition { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
