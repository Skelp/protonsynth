#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## PolyphonyManager Class
Represents simplified polyphony management of any number of [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') instances.  
```csharp
public class PolyphonyManager
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; PolyphonyManager  
### Constructors

***
[PolyphonyManager(IVoice[])](Skelp_ProtonSynth_PolyphonyManager_PolyphonyManager(Skelp_ProtonSynth_IVoice__).md 'Skelp.ProtonSynth.PolyphonyManager.PolyphonyManager(Skelp.ProtonSynth.IVoice[])')

Initializes a new instance of the [PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager') class.  
### Properties

***
[Amplitude](Skelp_ProtonSynth_PolyphonyManager_Amplitude.md 'Skelp.ProtonSynth.PolyphonyManager.Amplitude')

Gets or sets the maximum amplitude of samples from [GetSample()](Skelp_ProtonSynth_PolyphonyManager_GetSample().md 'Skelp.ProtonSynth.PolyphonyManager.GetSample()') and [GetSamples(int)](Skelp_ProtonSynth_PolyphonyManager_GetSamples(int).md 'Skelp.ProtonSynth.PolyphonyManager.GetSamples(int)').  

***
[Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices')

Gets or sets the voices the [PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager') manages.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_PolyphonyManager_GetSample().md 'Skelp.ProtonSynth.PolyphonyManager.GetSample()')

Processes all [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices') by calling their respective [GetSample()](Skelp_ProtonSynth_IVoice_GetSample().md 'Skelp.ProtonSynth.IVoice.GetSample()').  

***
[GetSamples(int)](Skelp_ProtonSynth_PolyphonyManager_GetSamples(int).md 'Skelp.ProtonSynth.PolyphonyManager.GetSamples(int)')

Processes all [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices') in parallel by calling their respective [GetSample()](Skelp_ProtonSynth_IVoice_GetSample().md 'Skelp.ProtonSynth.IVoice.GetSample()').  

***
[Panic()](Skelp_ProtonSynth_PolyphonyManager_Panic().md 'Skelp.ProtonSynth.PolyphonyManager.Panic()')

Stops all processing for all voices.  

***
[ProcessNoteOff(double)](Skelp_ProtonSynth_PolyphonyManager_ProcessNoteOff(double).md 'Skelp.ProtonSynth.PolyphonyManager.ProcessNoteOff(double)')

Takes in any note off signal and finds the oldest [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') which is still playing at a [frequency](Skelp_ProtonSynth_PolyphonyManager_ProcessNoteOff(double).md#Skelp_ProtonSynth_PolyphonyManager_ProcessNoteOff(double)_frequency 'Skelp.ProtonSynth.PolyphonyManager.ProcessNoteOff(double).frequency').  

***
[ProcessNoteOn(double, UnitInterval)](Skelp_ProtonSynth_PolyphonyManager_ProcessNoteOn(double_Skelp_ProtonSynth_UnitInterval).md 'Skelp.ProtonSynth.PolyphonyManager.ProcessNoteOn(double, Skelp.ProtonSynth.UnitInterval)')

Takes in any note off signal and forwards it to the oldest [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') in [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices').  

***
[ProcessPitchBend(UnitInterval)](Skelp_ProtonSynth_PolyphonyManager_ProcessPitchBend(Skelp_ProtonSynth_UnitInterval).md 'Skelp.ProtonSynth.PolyphonyManager.ProcessPitchBend(Skelp.ProtonSynth.UnitInterval)')

Processes pitchbend signals and forwards them to all [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') instances.  

***
[Reset()](Skelp_ProtonSynth_PolyphonyManager_Reset().md 'Skelp.ProtonSynth.PolyphonyManager.Reset()')

Resets Panic mode as well as anything the [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices') offer to be reset.  
