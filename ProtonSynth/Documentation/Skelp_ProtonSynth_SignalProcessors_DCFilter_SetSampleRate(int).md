#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter')
## DCFilter.SetSampleRate(int) Method
Sets the samplerate of the [DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter').  
```csharp
public void SetSampleRate(int sampleRate);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_DCFilter_SetSampleRate(int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate.
  
