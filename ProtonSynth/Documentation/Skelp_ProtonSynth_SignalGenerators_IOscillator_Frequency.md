#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator')
## IOscillator.Frequency Property
Gets or sets the frequency of the oscillator.  
```csharp
double Frequency { get; set; }
```
#### Property Value
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')
