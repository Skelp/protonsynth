#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend')
## PitchBend.PitchBend(double, int) Constructor
Initializes a new instance of the [PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend') class.  
```csharp
public PitchBend(double frequency, int semitoneRange);
```
#### Parameters
<a name='Skelp_ProtonSynth_PitchBend_PitchBend(double_int)_frequency'></a>
`frequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
The frequency the pitchbending should be based around.
  
<a name='Skelp_ProtonSynth_PitchBend_PitchBend(double_int)_semitoneRange'></a>
`semitoneRange` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
The range in which the pitch is bent [semitoneRange](Skelp_ProtonSynth_PitchBend_PitchBend(double_int).md#Skelp_ProtonSynth_PitchBend_PitchBend(double_int)_semitoneRange 'Skelp.ProtonSynth.PitchBend.PitchBend(double, int).semitoneRange') semitones in one direction.
  
