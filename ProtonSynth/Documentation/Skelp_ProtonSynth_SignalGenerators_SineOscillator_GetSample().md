#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[SineOscillator](Skelp_ProtonSynth_SignalGenerators_SineOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SineOscillator')
## SineOscillator.GetSample() Method
Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
```csharp
public override double GetSample();
```
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
A [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double') with a maximum range between -1 to 1.

Implements [GetSample()](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator.GetSample()')  
