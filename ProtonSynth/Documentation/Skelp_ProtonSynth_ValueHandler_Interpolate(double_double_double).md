#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[ValueHandler](Skelp_ProtonSynth_ValueHandler.md 'Skelp.ProtonSynth.ValueHandler')
## ValueHandler.Interpolate(double, double, double) Method
Calculates an interpolated value of [x1](Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double).md#Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_x1 'Skelp.ProtonSynth.ValueHandler.Interpolate(double, double, double).x1') and [x2](Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double).md#Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_x2 'Skelp.ProtonSynth.ValueHandler.Interpolate(double, double, double).x2') with the [ratio](Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double).md#Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_ratio 'Skelp.ProtonSynth.ValueHandler.Interpolate(double, double, double).ratio') provided.  
```csharp
public static double Interpolate(double x1, double x2, double ratio);
```
#### Parameters
<a name='Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_x1'></a>
`x1` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any value.
  
<a name='Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_x2'></a>
`x2` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any value.
  
<a name='Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_ratio'></a>
`ratio` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any number between 0 and 1 where 0 is 100% x1 and 1 is 100% x2.
  
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
The interpolated result of the operation.
