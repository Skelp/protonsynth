#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')
## PolyphonyManager.Voices Property
Gets or sets the voices the [PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager') manages.  
```csharp
public Skelp.ProtonSynth.IVoice[] Voices { get; set; }
```
#### Property Value
[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')[[]](https://docs.microsoft.com/en-us/dotnet/api/System.Array 'System.Array')
