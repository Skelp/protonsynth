#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer')
## SignalMixer.SignalMixer(IEnumerable&lt;ISignalGenerator&gt;) Constructor
Initializes a new instance of the [SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer') class.  
```csharp
public SignalMixer(System.Collections.Generic.IEnumerable<Skelp.ProtonSynth.SignalGenerators.ISignalGenerator> signalGenerators);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_SignalMixer_SignalMixer(System_Collections_Generic_IEnumerable_Skelp_ProtonSynth_SignalGenerators_ISignalGenerator_)_signalGenerators'></a>
`signalGenerators` [System.Collections.Generic.IEnumerable&lt;](https://docs.microsoft.com/en-us/dotnet/api/System.Collections.Generic.IEnumerable-1 'System.Collections.Generic.IEnumerable`1')[ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')[&gt;](https://docs.microsoft.com/en-us/dotnet/api/System.Collections.Generic.IEnumerable-1 'System.Collections.Generic.IEnumerable`1')  
Any collection of [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator') instances that is not NULL and has valid items.
  
