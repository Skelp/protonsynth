#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend')
## PitchBend.SemitoneRange Property
Gets or sets the value representing the difference in semitones from [Frequency](Skelp_ProtonSynth_PitchBend_Frequency.md 'Skelp.ProtonSynth.PitchBend.Frequency') in one direction.  
```csharp
public int SemitoneRange { get; set; }
```
#### Property Value
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')
### Remarks
The total semitone range of the [PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend') module is 2 * [SemitoneRange](Skelp_ProtonSynth_PitchBend_SemitoneRange.md 'Skelp.ProtonSynth.PitchBend.SemitoneRange') + 1.
