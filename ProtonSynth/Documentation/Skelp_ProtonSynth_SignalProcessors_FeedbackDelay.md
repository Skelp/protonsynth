#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors')
## FeedbackDelay Class
Provides a simple delay with feedback functionality.  
```csharp
public class FeedbackDelay :
Skelp.ProtonSynth.SignalProcessors.ISignalProcessor
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; FeedbackDelay  

Implements [ISignalProcessor](Skelp_ProtonSynth_SignalProcessors_ISignalProcessor.md 'Skelp.ProtonSynth.SignalProcessors.ISignalProcessor')  
### Constructors

***
[FeedbackDelay(int)](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_FeedbackDelay(int).md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.FeedbackDelay(int)')

Initializes a new instance of the [FeedbackDelay](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay') class.  
### Properties

***
[DelayTime](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_DelayTime.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.DelayTime')

Gets the time it takes for an input signal to be returned by the [FeedbackDelay](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay') instance.  

***
[Feedback](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_Feedback.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.Feedback')

The amount of feedback from the previous delay to be added back into the signal.  

***
[MaxAmplitude](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_MaxAmplitude.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.MaxAmplitude')

The maximum, relative amplitude to the original signal the initial delay will have.  

***
[SampleRate](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_SampleRate.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.SampleRate')

Gets the current samplerate the delay is running on.  
### Methods

***
[ProcessSample(double)](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_ProcessSample(double).md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.ProcessSample(double)')

Processes the given [sample](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_ProcessSample(double).md#Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_ProcessSample(double)_sample 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.ProcessSample(double).sample').  

***
[Resize(TimeSpan)](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_Resize(System_TimeSpan).md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.Resize(System.TimeSpan)')

Resizes the delay internally to fit the new [delayTime](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_Resize(System_TimeSpan).md#Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_Resize(System_TimeSpan)_delayTime 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.Resize(System.TimeSpan).delayTime').  

***
[SetSampleRate(int)](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_SetSampleRate(int).md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.SetSampleRate(int)')

Updates the samplerate of the delay unit.  
