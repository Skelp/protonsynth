#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')
## Oscillator.SetSampleRate(int) Method
Sets the samplerate of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  
```csharp
public void SetSampleRate(int sampleRate);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_Oscillator_SetSampleRate(int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate.
  

Implements [SetSampleRate(int)](Skelp_ProtonSynth_SignalGenerators_IOscillator_SetSampleRate(int).md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.SetSampleRate(int)')  
