#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter')
## DCFilter.DCFilter(int) Constructor
Initializes a new instance of the [DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter') class.  
```csharp
public DCFilter(int sampleRate);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_DCFilter_DCFilter(int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate more than zero.
  
