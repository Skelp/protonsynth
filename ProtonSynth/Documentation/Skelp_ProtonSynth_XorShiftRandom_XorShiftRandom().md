#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[XorShiftRandom](Skelp_ProtonSynth_XorShiftRandom.md 'Skelp.ProtonSynth.XorShiftRandom')
## XorShiftRandom.XorShiftRandom() Constructor
Constructs a new  generator using two  
random Guid hash codes as a seed.  
```csharp
public XorShiftRandom();
```
