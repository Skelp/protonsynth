#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[RampOscillator](Skelp_ProtonSynth_SignalGenerators_RampOscillator.md 'Skelp.ProtonSynth.SignalGenerators.RampOscillator')
## RampOscillator.RampOscillator(int, double, double) Constructor
Initializes a new instance of the [RampOscillator](Skelp_ProtonSynth_SignalGenerators_RampOscillator.md 'Skelp.ProtonSynth.SignalGenerators.RampOscillator') class.  
```csharp
public RampOscillator(int sampleRate, double frequency=440.0, double amplitudeMultiplier=1.0);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_RampOscillator_RampOscillator(int_double_double)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate more than zero.
  
<a name='Skelp_ProtonSynth_SignalGenerators_RampOscillator_RampOscillator(int_double_double)_frequency'></a>
`frequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive frequency more than zero.
  
<a name='Skelp_ProtonSynth_SignalGenerators_RampOscillator_RampOscillator(int_double_double)_amplitudeMultiplier'></a>
`amplitudeMultiplier` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any value between [MinValue](Skelp_ProtonSynth_UnitInterval_MinValue.md 'Skelp.ProtonSynth.UnitInterval.MinValue') and [MaxValue](Skelp_ProtonSynth_UnitInterval_MaxValue.md 'Skelp.ProtonSynth.UnitInterval.MaxValue').
  
