#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer')
## SignalMixer.SignalMixer(ISignalGenerator[]) Constructor
Initializes a new instance of the [SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer') class.  
```csharp
public SignalMixer(params Skelp.ProtonSynth.SignalGenerators.ISignalGenerator[] signalGenerators);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_SignalMixer_SignalMixer(Skelp_ProtonSynth_SignalGenerators_ISignalGenerator__)_signalGenerators'></a>
`signalGenerators` [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')[[]](https://docs.microsoft.com/en-us/dotnet/api/System.Array 'System.Array')  
Any amount of [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator') instances that is not NULL and has valid items.
  
