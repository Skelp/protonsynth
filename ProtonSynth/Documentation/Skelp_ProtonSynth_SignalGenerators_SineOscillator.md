#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## SineOscillator Class
Implementation of a sine [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  
```csharp
public class SineOscillator : Skelp.ProtonSynth.SignalGenerators.Oscillator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') &#129106; SineOscillator  
### Constructors

***
[SineOscillator(int, double, double)](Skelp_ProtonSynth_SignalGenerators_SineOscillator_SineOscillator(int_double_double).md 'Skelp.ProtonSynth.SignalGenerators.SineOscillator.SineOscillator(int, double, double)')

Initializes a new instance of the [SineOscillator](Skelp_ProtonSynth_SignalGenerators_SineOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SineOscillator') class.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_SineOscillator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.SineOscillator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
