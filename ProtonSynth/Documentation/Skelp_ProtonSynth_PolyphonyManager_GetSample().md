#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')
## PolyphonyManager.GetSample() Method
Processes all [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices') by calling their respective [GetSample()](Skelp_ProtonSynth_IVoice_GetSample().md 'Skelp.ProtonSynth.IVoice.GetSample()').  
```csharp
public double GetSample();
```
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
The summed sample of all [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices') multiplied by [Amplitude](Skelp_ProtonSynth_PolyphonyManager_Amplitude.md 'Skelp.ProtonSynth.PolyphonyManager.Amplitude').
