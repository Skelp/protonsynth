#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')
## PolyphonyManager.Amplitude Property
Gets or sets the maximum amplitude of samples from [GetSample()](Skelp_ProtonSynth_PolyphonyManager_GetSample().md 'Skelp.ProtonSynth.PolyphonyManager.GetSample()') and [GetSamples(int)](Skelp_ProtonSynth_PolyphonyManager_GetSamples(int).md 'Skelp.ProtonSynth.PolyphonyManager.GetSamples(int)').  
```csharp
public Skelp.ProtonSynth.UnitInterval Amplitude { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
