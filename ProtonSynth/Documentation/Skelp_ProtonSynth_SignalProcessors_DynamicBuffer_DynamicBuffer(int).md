#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer')
## DynamicBuffer.DynamicBuffer(int) Constructor
Initializes a new instance of the [DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer') class.  
```csharp
public DynamicBuffer(int capacity);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_DynamicBuffer_DynamicBuffer(int)_capacity'></a>
`capacity` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
The initial size of the buffer.
  
