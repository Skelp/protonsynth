#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## UnitInterval Struct
Represents a real number between 0 and 1.  
```csharp
public struct UnitInterval
```
### Fields

***
[MaxValue](Skelp_ProtonSynth_UnitInterval_MaxValue.md 'Skelp.ProtonSynth.UnitInterval.MaxValue')

Represents the maximum value a [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval') can hold.  

***
[MinValue](Skelp_ProtonSynth_UnitInterval_MinValue.md 'Skelp.ProtonSynth.UnitInterval.MinValue')

Represents the minimum value a [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval') can hold.  
