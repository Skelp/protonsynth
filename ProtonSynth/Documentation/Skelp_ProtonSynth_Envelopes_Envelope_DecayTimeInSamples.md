#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.DecayTimeInSamples Property
Gets or sets the decay time in samples based on [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').  
```csharp
private protected int DecayTimeInSamples { get; set; }
```
#### Property Value
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')
