#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[LinearEnvelope](Skelp_ProtonSynth_Envelopes_LinearEnvelope.md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope')
## LinearEnvelope.GetAmplitude() Method
Gets the next sample according to the inner state of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
```csharp
public override Skelp.ProtonSynth.UnitInterval GetAmplitude();
```
#### Returns
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')  
A [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval').

Implements [GetAmplitude()](Skelp_ProtonSynth_Envelopes_IEnvelope_GetAmplitude().md 'Skelp.ProtonSynth.Envelopes.IEnvelope.GetAmplitude()')  
