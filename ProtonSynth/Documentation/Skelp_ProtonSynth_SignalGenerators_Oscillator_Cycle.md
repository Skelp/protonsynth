#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')
## Oscillator.Cycle Property
Gets or sets the current position of the [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') relative to [Frequency](Skelp_ProtonSynth_SignalGenerators_Oscillator_Frequency.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Frequency').  
```csharp
private protected double Cycle { get; set; }
```
#### Property Value
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')
