#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope')
## IEnvelope.SustainAmplitude Property
Gets or sets the amplitude the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') maintains when [DecayTime](Skelp_ProtonSynth_Envelopes_IEnvelope_DecayTime.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.DecayTime') is over.  
```csharp
Skelp.ProtonSynth.UnitInterval SustainAmplitude { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
