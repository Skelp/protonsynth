#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## FastSineOscillator Class
Implementation of an approximation of a sine [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  


  
Reduces computation time but introduces harmonics.  
```csharp
public class FastSineOscillator : Skelp.ProtonSynth.SignalGenerators.Oscillator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') &#129106; FastSineOscillator  
### Constructors

***
[FastSineOscillator(int, double, double)](Skelp_ProtonSynth_SignalGenerators_FastSineOscillator_FastSineOscillator(int_double_double).md 'Skelp.ProtonSynth.SignalGenerators.FastSineOscillator.FastSineOscillator(int, double, double)')

Initializes a new instance of the [FastSineOscillator](Skelp_ProtonSynth_SignalGenerators_FastSineOscillator.md 'Skelp.ProtonSynth.SignalGenerators.FastSineOscillator') class.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_FastSineOscillator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.FastSineOscillator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
