#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')
## Oscillator.CalculateCycle() Method
Advances [Cycle](Skelp_ProtonSynth_SignalGenerators_Oscillator_Cycle.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Cycle') for the next sample in accordance with [Frequency](Skelp_ProtonSynth_SignalGenerators_Oscillator_Frequency.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Frequency') and [SampleRate](Skelp_ProtonSynth_SignalGenerators_Oscillator_SampleRate.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.SampleRate').  
```csharp
private protected void CalculateCycle();
```
