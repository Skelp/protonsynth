#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter')
## Filter.SampleRate Property
Gets the samplerate of the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter').  
```csharp
public int SampleRate { get; set; }
```
#### Property Value
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')
