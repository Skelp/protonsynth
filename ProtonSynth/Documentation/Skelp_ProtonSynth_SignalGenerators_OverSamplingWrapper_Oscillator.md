#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')
## OverSamplingWrapper.Oscillator Property
Gets the [Oscillator](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.Oscillator') which is being oversampled in [GetSample()](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.GetSample()').  
```csharp
public Skelp.ProtonSynth.SignalGenerators.IOscillator Oscillator { get; set; }
```
#### Property Value
[IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator')
