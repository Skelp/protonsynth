#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')
## IVoice.ProcessNoteOn(double, UnitInterval) Method
Aligns the inner state of the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') and processes incoming note on signals.  
```csharp
void ProcessNoteOn(double frequency, Skelp.ProtonSynth.UnitInterval amplitude);
```
#### Parameters
<a name='Skelp_ProtonSynth_IVoice_ProcessNoteOn(double_Skelp_ProtonSynth_UnitInterval)_frequency'></a>
`frequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive frequency more than zero.
  
<a name='Skelp_ProtonSynth_IVoice_ProcessNoteOn(double_Skelp_ProtonSynth_UnitInterval)_amplitude'></a>
`amplitude` [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')  
Any value between [MinValue](Skelp_ProtonSynth_UnitInterval_MinValue.md 'Skelp.ProtonSynth.UnitInterval.MinValue') and [MaxValue](Skelp_ProtonSynth_UnitInterval_MaxValue.md 'Skelp.ProtonSynth.UnitInterval.MaxValue').
  
