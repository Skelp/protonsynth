#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')
## OverSamplingWrapper.OverSamplingWrapper(IOscillator, int, int) Constructor
Initializes a new instance of the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') class.  
```csharp
public OverSamplingWrapper(Skelp.ProtonSynth.SignalGenerators.IOscillator oscillator, int sampleRate, int overSamplingFactor);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_OverSamplingWrapper(Skelp_ProtonSynth_SignalGenerators_IOscillator_int_int)_oscillator'></a>
`oscillator` [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator')  
Any oscillator.
  
<a name='Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_OverSamplingWrapper(Skelp_ProtonSynth_SignalGenerators_IOscillator_int_int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate more than zero.
  
<a name='Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_OverSamplingWrapper(Skelp_ProtonSynth_SignalGenerators_IOscillator_int_int)_overSamplingFactor'></a>
`overSamplingFactor` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive factor.
  
