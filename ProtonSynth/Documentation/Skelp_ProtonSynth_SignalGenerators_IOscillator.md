#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## IOscillator Interface
An interface that describes any oscillating signal source.  
```csharp
public interface IOscillator :
Skelp.ProtonSynth.SignalGenerators.ISignalGenerator
```

Derived  
&#8627; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')  

Implements [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')  
### Properties

***
[Frequency](Skelp_ProtonSynth_SignalGenerators_IOscillator_Frequency.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.Frequency')

Gets or sets the frequency of the oscillator.  

***
[Phase](Skelp_ProtonSynth_SignalGenerators_IOscillator_Phase.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.Phase')

Gets or sets the phase of the oscillator where a value of 0 represents 0 degrees and a value of 1 represents 360 degrees.  

***
[SampleRate](Skelp_ProtonSynth_SignalGenerators_IOscillator_SampleRate.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.SampleRate')

Gets the samplerate of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  
### Methods

***
[ResetCycle()](Skelp_ProtonSynth_SignalGenerators_IOscillator_ResetCycle().md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.ResetCycle()')

Resets the internal cycle of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  

***
[SetSampleRate(int)](Skelp_ProtonSynth_SignalGenerators_IOscillator_SetSampleRate(int).md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.SetSampleRate(int)')

Sets the samplerate of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  
