#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## IVoice Interface
Represents any constructed synthesizer voice to be used in a polymorphic synthesizer.  
```csharp
public interface IVoice
```
### Remarks
Suggested application for this interface is in conjunction with [PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager').
### Properties

***
[CurrentFrequency](Skelp_ProtonSynth_IVoice_CurrentFrequency.md 'Skelp.ProtonSynth.IVoice.CurrentFrequency')

Gets the frequency that was last sent to the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').  

***
[IsPlaying](Skelp_ProtonSynth_IVoice_IsPlaying.md 'Skelp.ProtonSynth.IVoice.IsPlaying')

Gets a value indicating whether [ProcessNoteOn(double, UnitInterval)](Skelp_ProtonSynth_IVoice_ProcessNoteOn(double_Skelp_ProtonSynth_UnitInterval).md 'Skelp.ProtonSynth.IVoice.ProcessNoteOn(double, Skelp.ProtonSynth.UnitInterval)') or [ProcessNoteOff()](Skelp_ProtonSynth_IVoice_ProcessNoteOff().md 'Skelp.ProtonSynth.IVoice.ProcessNoteOff()') was last called.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_IVoice_GetSample().md 'Skelp.ProtonSynth.IVoice.GetSample()')

Processes the entire pipeline of the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').  

***
[GetSamples(int)](Skelp_ProtonSynth_IVoice_GetSamples(int).md 'Skelp.ProtonSynth.IVoice.GetSamples(int)')

Processes the entire pipeline of the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').  

***
[ProcessNoteOff()](Skelp_ProtonSynth_IVoice_ProcessNoteOff().md 'Skelp.ProtonSynth.IVoice.ProcessNoteOff()')

Processes the note off signal by setting the state of relevant members in [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').  

***
[ProcessNoteOn(double, UnitInterval)](Skelp_ProtonSynth_IVoice_ProcessNoteOn(double_Skelp_ProtonSynth_UnitInterval).md 'Skelp.ProtonSynth.IVoice.ProcessNoteOn(double, Skelp.ProtonSynth.UnitInterval)')

Aligns the inner state of the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') and processes incoming note on signals.  

***
[ProcessPitchBend(UnitInterval)](Skelp_ProtonSynth_IVoice_ProcessPitchBend(Skelp_ProtonSynth_UnitInterval).md 'Skelp.ProtonSynth.IVoice.ProcessPitchBend(Skelp.ProtonSynth.UnitInterval)')

Processes incoming pitchbend signals.  
