#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors')
## ISignalProcessor Interface
An interface that describes any signal altering unit.  
```csharp
public interface ISignalProcessor
```

Derived  
&#8627; [DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter')  
&#8627; [FeedbackDelay](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay')  
&#8627; [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter')  
### Methods

***
[ProcessSample(double)](Skelp_ProtonSynth_SignalProcessors_ISignalProcessor_ProcessSample(double).md 'Skelp.ProtonSynth.SignalProcessors.ISignalProcessor.ProcessSample(double)')

Processes the given [sample](Skelp_ProtonSynth_SignalProcessors_ISignalProcessor_ProcessSample(double).md#Skelp_ProtonSynth_SignalProcessors_ISignalProcessor_ProcessSample(double)_sample 'Skelp.ProtonSynth.SignalProcessors.ISignalProcessor.ProcessSample(double).sample').  
