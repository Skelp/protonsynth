#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')
## Oscillator.SampleRate Property
Gets the samplerate of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  
```csharp
public int SampleRate { get; set; }
```
#### Property Value
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')

Implements [SampleRate](Skelp_ProtonSynth_SignalGenerators_IOscillator_SampleRate.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.SampleRate')  
