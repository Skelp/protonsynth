#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[MidiFrequencyArray](Skelp_ProtonSynth_MidiFrequencyArray.md 'Skelp.ProtonSynth.MidiFrequencyArray')
## MidiFrequencyArray.this[int] Property
Gets the corresponding frequency in Hz.  
```csharp
public double this[int midiNote] { get; }
```
#### Parameters
<a name='Skelp_ProtonSynth_MidiFrequencyArray_this_int__midiNote'></a>
`midiNote` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any valid midi note from 0 to 127.
  
#### Property Value
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')
