#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend')
## PitchBend.BendPosition Property
Gets or sets the relative position of the [PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend'). 0.5 represents a neutral position.  
```csharp
public Skelp.ProtonSynth.UnitInterval BendPosition { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
