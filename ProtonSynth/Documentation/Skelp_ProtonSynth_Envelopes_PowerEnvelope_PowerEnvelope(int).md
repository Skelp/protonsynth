#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[PowerEnvelope](Skelp_ProtonSynth_Envelopes_PowerEnvelope.md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope')
## PowerEnvelope.PowerEnvelope(int) Constructor
Initializes a new instance of the [PowerEnvelope](Skelp_ProtonSynth_Envelopes_PowerEnvelope.md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope') class.  
```csharp
public PowerEnvelope(int sampleRate=44100);
```
#### Parameters
<a name='Skelp_ProtonSynth_Envelopes_PowerEnvelope_PowerEnvelope(int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
The samplerate for the new [PowerEnvelope](Skelp_ProtonSynth_Envelopes_PowerEnvelope.md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope').
  
