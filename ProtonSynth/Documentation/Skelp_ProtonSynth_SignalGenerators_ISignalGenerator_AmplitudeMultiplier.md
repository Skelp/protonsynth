#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')
## ISignalGenerator.AmplitudeMultiplier Property
Gets or sets the maximum amplitude of the signal.  
```csharp
Skelp.ProtonSynth.UnitInterval AmplitudeMultiplier { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
