#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope')
## IEnvelope.TriggerOn() Method
Triggers the attack phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
```csharp
void TriggerOn();
```
