#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors')
## Filter Class
Filter based off of the original C code on  which implements a digital filter as seen in the RBJ Audio-EQ-Cookbook.  
```csharp
public class Filter :
Skelp.ProtonSynth.SignalProcessors.ISignalProcessor
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; Filter  

Implements [ISignalProcessor](Skelp_ProtonSynth_SignalProcessors_ISignalProcessor.md 'Skelp.ProtonSynth.SignalProcessors.ISignalProcessor')  
### Constructors

***
[Filter(int, FilterType, double, double)](Skelp_ProtonSynth_SignalProcessors_Filter_Filter(int_Skelp_ProtonSynth_SignalProcessors_FilterType_double_double).md 'Skelp.ProtonSynth.SignalProcessors.Filter.Filter(int, Skelp.ProtonSynth.SignalProcessors.FilterType, double, double)')

Initializes a new instance of the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') class.  
### Properties

***
[SampleRate](Skelp_ProtonSynth_SignalProcessors_Filter_SampleRate.md 'Skelp.ProtonSynth.SignalProcessors.Filter.SampleRate')

Gets the samplerate of the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter').  
### Methods

***
[ProcessSample(double)](Skelp_ProtonSynth_SignalProcessors_Filter_ProcessSample(double).md 'Skelp.ProtonSynth.SignalProcessors.Filter.ProcessSample(double)')

Processes the given [sample](Skelp_ProtonSynth_SignalProcessors_Filter_ProcessSample(double).md#Skelp_ProtonSynth_SignalProcessors_Filter_ProcessSample(double)_sample 'Skelp.ProtonSynth.SignalProcessors.Filter.ProcessSample(double).sample').  

***
[SetAll(FilterType, double, double)](Skelp_ProtonSynth_SignalProcessors_Filter_SetAll(Skelp_ProtonSynth_SignalProcessors_FilterType_double_double).md 'Skelp.ProtonSynth.SignalProcessors.Filter.SetAll(Skelp.ProtonSynth.SignalProcessors.FilterType, double, double)')

Sets the parameters and adjusts the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') accordingly.  

***
[SetFrequency(double)](Skelp_ProtonSynth_SignalProcessors_Filter_SetFrequency(double).md 'Skelp.ProtonSynth.SignalProcessors.Filter.SetFrequency(double)')

Sets the current frequency and adjusts the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') accordingly.  

***
[SetQ(double)](Skelp_ProtonSynth_SignalProcessors_Filter_SetQ(double).md 'Skelp.ProtonSynth.SignalProcessors.Filter.SetQ(double)')

Sets the current Q and adjusts the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') accordingly.  

***
[SetSampleRate(int)](Skelp_ProtonSynth_SignalProcessors_Filter_SetSampleRate(int).md 'Skelp.ProtonSynth.SignalProcessors.Filter.SetSampleRate(int)')

Sets the samplerate of the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter').  

***
[SetType(FilterType)](Skelp_ProtonSynth_SignalProcessors_Filter_SetType(Skelp_ProtonSynth_SignalProcessors_FilterType).md 'Skelp.ProtonSynth.SignalProcessors.Filter.SetType(Skelp.ProtonSynth.SignalProcessors.FilterType)')

Sets the current frequency and adjusts the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') accordingly.  
