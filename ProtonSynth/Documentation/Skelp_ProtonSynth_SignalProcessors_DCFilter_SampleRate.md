#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter')
## DCFilter.SampleRate Property
Gets the samplerate of the [DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter').  
```csharp
public int SampleRate { get; set; }
```
#### Property Value
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')
