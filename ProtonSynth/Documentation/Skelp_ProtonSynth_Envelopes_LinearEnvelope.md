#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes')
## LinearEnvelope Class
Implementation of a linear ADSR [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
```csharp
public class LinearEnvelope : Skelp.ProtonSynth.Envelopes.Envelope
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope') &#129106; LinearEnvelope  

Derived  
&#8627; [PowerEnvelope](Skelp_ProtonSynth_Envelopes_PowerEnvelope.md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope')  
### Constructors

***
[LinearEnvelope(int)](Skelp_ProtonSynth_Envelopes_LinearEnvelope_LinearEnvelope(int).md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope.LinearEnvelope(int)')

Initializes a new instance of the [LinearEnvelope](Skelp_ProtonSynth_Envelopes_LinearEnvelope.md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope') class.  

***
[LinearEnvelope(TimeSpan, TimeSpan, UnitInterval, TimeSpan, int)](Skelp_ProtonSynth_Envelopes_LinearEnvelope_LinearEnvelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int).md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope.LinearEnvelope(System.TimeSpan, System.TimeSpan, Skelp.ProtonSynth.UnitInterval, System.TimeSpan, int)')

Initializes a new instance of the [LinearEnvelope](Skelp_ProtonSynth_Envelopes_LinearEnvelope.md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope') class.  
### Methods

***
[GetAmplitude()](Skelp_ProtonSynth_Envelopes_LinearEnvelope_GetAmplitude().md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope.GetAmplitude()')

Gets the next sample according to the inner state of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
