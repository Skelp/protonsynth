#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[WavetableOscillator](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator')
## WavetableOscillator.WaveformTable Property
Gets or sets the current waveform table of the [WavetableOscillator](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator') of any given size.  
```csharp
public Skelp.ProtonSynth.Waveform[] WaveformTable { get; set; }
```
#### Property Value
[Waveform](Skelp_ProtonSynth_Waveform.md 'Skelp.ProtonSynth.Waveform')[[]](https://docs.microsoft.com/en-us/dotnet/api/System.Array 'System.Array')
