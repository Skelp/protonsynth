#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')
## OverSamplingWrapper.SetSampleRate(int) Method
Updates the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') according to the new [sampleRate](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetSampleRate(int).md#Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetSampleRate(int)_sampleRate 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.SetSampleRate(int).sampleRate').  
```csharp
public void SetSampleRate(int sampleRate);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetSampleRate(int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate more than zero.
  
