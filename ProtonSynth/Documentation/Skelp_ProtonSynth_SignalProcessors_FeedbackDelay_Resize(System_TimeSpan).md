#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[FeedbackDelay](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay')
## FeedbackDelay.Resize(TimeSpan) Method
Resizes the delay internally to fit the new [delayTime](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_Resize(System_TimeSpan).md#Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_Resize(System_TimeSpan)_delayTime 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.Resize(System.TimeSpan).delayTime').  
```csharp
public void Resize(System.TimeSpan delayTime);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_Resize(System_TimeSpan)_delayTime'></a>
`delayTime` [System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan')  
Any valid [System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan').
  
