#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## ValueHandler Class
```csharp
public static class ValueHandler
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; ValueHandler  
### Methods

***
[Interpolate(double, double, double)](Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double).md 'Skelp.ProtonSynth.ValueHandler.Interpolate(double, double, double)')

Calculates an interpolated value of [x1](Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double).md#Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_x1 'Skelp.ProtonSynth.ValueHandler.Interpolate(double, double, double).x1') and [x2](Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double).md#Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_x2 'Skelp.ProtonSynth.ValueHandler.Interpolate(double, double, double).x2') with the [ratio](Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double).md#Skelp_ProtonSynth_ValueHandler_Interpolate(double_double_double)_ratio 'Skelp.ProtonSynth.ValueHandler.Interpolate(double, double, double).ratio') provided.  
