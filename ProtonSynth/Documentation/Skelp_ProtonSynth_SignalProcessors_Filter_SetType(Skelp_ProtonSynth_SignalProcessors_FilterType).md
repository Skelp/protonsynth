#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter')
## Filter.SetType(FilterType) Method
Sets the current frequency and adjusts the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') accordingly.  
```csharp
public void SetType(Skelp.ProtonSynth.SignalProcessors.FilterType filterType);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_SetType(Skelp_ProtonSynth_SignalProcessors_FilterType)_filterType'></a>
`filterType` [FilterType](Skelp_ProtonSynth_SignalProcessors_FilterType.md 'Skelp.ProtonSynth.SignalProcessors.FilterType')  
Any [FilterType](Skelp_ProtonSynth_SignalProcessors_FilterType.md 'Skelp.ProtonSynth.SignalProcessors.FilterType').
  
