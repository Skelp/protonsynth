#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.StartAmplitude Property
Gets or sets the amplitude the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') starts with once when triggered on.  
```csharp
public Skelp.ProtonSynth.UnitInterval StartAmplitude { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')

Implements [StartAmplitude](Skelp_ProtonSynth_Envelopes_IEnvelope_StartAmplitude.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.StartAmplitude')  
