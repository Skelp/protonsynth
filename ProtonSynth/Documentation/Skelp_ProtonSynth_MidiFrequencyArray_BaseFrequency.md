#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[MidiFrequencyArray](Skelp_ProtonSynth_MidiFrequencyArray.md 'Skelp.ProtonSynth.MidiFrequencyArray')
## MidiFrequencyArray.BaseFrequency Property
Gets the frequency the array was originally constructed with.  
```csharp
public double BaseFrequency { get; }
```
#### Property Value
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')
