#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.AmplitudeMultiplier Property
Gets or sets the maximum value of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
```csharp
public Skelp.ProtonSynth.UnitInterval AmplitudeMultiplier { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')

Implements [AmplitudeMultiplier](Skelp_ProtonSynth_Envelopes_IEnvelope_AmplitudeMultiplier.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.AmplitudeMultiplier')  
