#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')
## PolyphonyManager.PolyphonyManager(IVoice[]) Constructor
Initializes a new instance of the [PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager') class.  
```csharp
public PolyphonyManager(params Skelp.ProtonSynth.IVoice[] voices);
```
#### Parameters
<a name='Skelp_ProtonSynth_PolyphonyManager_PolyphonyManager(Skelp_ProtonSynth_IVoice__)_voices'></a>
`voices` [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')[[]](https://docs.microsoft.com/en-us/dotnet/api/System.Array 'System.Array')  
At least one [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') instance.
  
