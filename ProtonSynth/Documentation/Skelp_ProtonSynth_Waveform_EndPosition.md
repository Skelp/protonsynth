#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[Waveform](Skelp_ProtonSynth_Waveform.md 'Skelp.ProtonSynth.Waveform')
## Waveform.EndPosition Property
Gets or sets the last position within the cycle before going back to [StartPosition](Skelp_ProtonSynth_Waveform_StartPosition.md 'Skelp.ProtonSynth.Waveform.StartPosition').  
```csharp
public Skelp.ProtonSynth.UnitInterval EndPosition { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
