#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[XorShiftRandom](Skelp_ProtonSynth_XorShiftRandom.md 'Skelp.ProtonSynth.XorShiftRandom')
## XorShiftRandom.NextDouble() Method
Generates a pseudorandom double between  
0 and 1 non-inclusive.  
```csharp
public double NextDouble();
```
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
A pseudorandom double.  
