#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes')
## PowerEnvelope Class
Implementation of an ADSR [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') based on [LinearEnvelope](Skelp_ProtonSynth_Envelopes_LinearEnvelope.md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope').  
```csharp
public class PowerEnvelope : Skelp.ProtonSynth.Envelopes.LinearEnvelope
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope') &#129106; [LinearEnvelope](Skelp_ProtonSynth_Envelopes_LinearEnvelope.md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope') &#129106; PowerEnvelope  
### Constructors

***
[PowerEnvelope(int)](Skelp_ProtonSynth_Envelopes_PowerEnvelope_PowerEnvelope(int).md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope.PowerEnvelope(int)')

Initializes a new instance of the [PowerEnvelope](Skelp_ProtonSynth_Envelopes_PowerEnvelope.md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope') class.  

***
[PowerEnvelope(TimeSpan, TimeSpan, UnitInterval, TimeSpan, int)](Skelp_ProtonSynth_Envelopes_PowerEnvelope_PowerEnvelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int).md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope.PowerEnvelope(System.TimeSpan, System.TimeSpan, Skelp.ProtonSynth.UnitInterval, System.TimeSpan, int)')

Initializes a new instance of the [PowerEnvelope](Skelp_ProtonSynth_Envelopes_PowerEnvelope.md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope') class.  
### Methods

***
[GetAmplitude()](Skelp_ProtonSynth_Envelopes_PowerEnvelope_GetAmplitude().md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope.GetAmplitude()')

Gets the next sample according to the inner state of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
