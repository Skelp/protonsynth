#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')
## IVoice.ProcessPitchBend(UnitInterval) Method
Processes incoming pitchbend signals.  
```csharp
void ProcessPitchBend(Skelp.ProtonSynth.UnitInterval bendPosition);
```
#### Parameters
<a name='Skelp_ProtonSynth_IVoice_ProcessPitchBend(Skelp_ProtonSynth_UnitInterval)_bendPosition'></a>
`bendPosition` [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')  
Any valid [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval'). Value 0.5 represents the middle of the [PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend').
  
