#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')
## IVoice.GetSample() Method
Processes the entire pipeline of the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').  
```csharp
double GetSample();
```
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
The next sample according to the inner state of the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').
