#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## MidiFrequencyArray Class
Provides an array of frequencies for every midi note from 0 to 127.  
```csharp
public class MidiFrequencyArray
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; MidiFrequencyArray  
### Constructors

***
[MidiFrequencyArray(double)](Skelp_ProtonSynth_MidiFrequencyArray_MidiFrequencyArray(double).md 'Skelp.ProtonSynth.MidiFrequencyArray.MidiFrequencyArray(double)')

Initializes a new instance of the [MidiFrequencyArray](Skelp_ProtonSynth_MidiFrequencyArray.md 'Skelp.ProtonSynth.MidiFrequencyArray') class.  
### Properties

***
[BaseFrequency](Skelp_ProtonSynth_MidiFrequencyArray_BaseFrequency.md 'Skelp.ProtonSynth.MidiFrequencyArray.BaseFrequency')

Gets the frequency the array was originally constructed with.  

***
[this[int]](Skelp_ProtonSynth_MidiFrequencyArray_this_int_.md 'Skelp.ProtonSynth.MidiFrequencyArray.this[int]')

Gets the corresponding frequency in Hz.  
