#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.Envelope(TimeSpan, TimeSpan, UnitInterval, TimeSpan, int) Constructor
Initializes a new instance of the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope') class.  
```csharp
public Envelope(System.TimeSpan attackTime, System.TimeSpan decayTime, Skelp.ProtonSynth.UnitInterval sustainAmplitude, System.TimeSpan releaseTime, int sampleRate=44100);
```
#### Parameters
<a name='Skelp_ProtonSynth_Envelopes_Envelope_Envelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int)_attackTime'></a>
`attackTime` [System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan')  
Relative time spent in the attack phase.
  
<a name='Skelp_ProtonSynth_Envelopes_Envelope_Envelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int)_decayTime'></a>
`decayTime` [System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan')  
Relative time spent in the decay phase.
  
<a name='Skelp_ProtonSynth_Envelopes_Envelope_Envelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int)_sustainAmplitude'></a>
`sustainAmplitude` [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')  
The amplitude being held when [decayTime](Skelp_ProtonSynth_Envelopes_Envelope_Envelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int).md#Skelp_ProtonSynth_Envelopes_Envelope_Envelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int)_decayTime 'Skelp.ProtonSynth.Envelopes.Envelope.Envelope(System.TimeSpan, System.TimeSpan, Skelp.ProtonSynth.UnitInterval, System.TimeSpan, int).decayTime') is over.
  
<a name='Skelp_ProtonSynth_Envelopes_Envelope_Envelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int)_releaseTime'></a>
`releaseTime` [System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan')  
Relative time spent in the release phase.
  
<a name='Skelp_ProtonSynth_Envelopes_Envelope_Envelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
The samplerate for the new [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope').
  
