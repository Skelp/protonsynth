#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[FeedbackDelay](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay')
## FeedbackDelay.ProcessSample(double) Method
Processes the given [sample](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_ProcessSample(double).md#Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_ProcessSample(double)_sample 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.ProcessSample(double).sample').  
```csharp
public double ProcessSample(double sample);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_ProcessSample(double)_sample'></a>
`sample` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any value between -1 and 1.
  
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
The processed [sample](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_ProcessSample(double).md#Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_ProcessSample(double)_sample 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay.ProcessSample(double).sample').

Implements [ProcessSample(double)](Skelp_ProtonSynth_SignalProcessors_ISignalProcessor_ProcessSample(double).md 'Skelp.ProtonSynth.SignalProcessors.ISignalProcessor.ProcessSample(double)')  
