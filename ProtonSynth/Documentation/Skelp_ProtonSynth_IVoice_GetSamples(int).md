#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')
## IVoice.GetSamples(int) Method
Processes the entire pipeline of the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').  
```csharp
double[] GetSamples(int amount);
```
#### Parameters
<a name='Skelp_ProtonSynth_IVoice_GetSamples(int)_amount'></a>
`amount` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
  
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')[[]](https://docs.microsoft.com/en-us/dotnet/api/System.Array 'System.Array')  
The next n-samples according to the inner state of the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').
