#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter')
## Filter.SetQ(double) Method
Sets the current Q and adjusts the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') accordingly.  
```csharp
public void SetQ(double q);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_SetQ(double)_q'></a>
`q` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive Q (quality factor).
  
