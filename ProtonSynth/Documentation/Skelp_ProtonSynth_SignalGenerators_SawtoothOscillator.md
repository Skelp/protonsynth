#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## SawtoothOscillator Class
Implementation of a sawtooth [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  
```csharp
public class SawtoothOscillator : Skelp.ProtonSynth.SignalGenerators.Oscillator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') &#129106; SawtoothOscillator  

Derived  
&#8627; [RampOscillator](Skelp_ProtonSynth_SignalGenerators_RampOscillator.md 'Skelp.ProtonSynth.SignalGenerators.RampOscillator')  
### Constructors

***
[SawtoothOscillator(int, double, double)](Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator_SawtoothOscillator(int_double_double).md 'Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator.SawtoothOscillator(int, double, double)')

Initializes a new instance of the [SawtoothOscillator](Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator') class.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
