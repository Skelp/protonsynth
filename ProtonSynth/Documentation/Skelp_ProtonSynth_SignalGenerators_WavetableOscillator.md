#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## WavetableOscillator Class
Implementation of a wavetable [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  
```csharp
public class WavetableOscillator : Skelp.ProtonSynth.SignalGenerators.Oscillator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') &#129106; WavetableOscillator  
### Constructors

***
[WavetableOscillator(int, double, double)](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator_WavetableOscillator(int_double_double).md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator.WavetableOscillator(int, double, double)')

Initializes a new instance of the [WavetableOscillator](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator') class.  
### Properties

***
[TablePosition](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator_TablePosition.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator.TablePosition')

Gets or sets the position within the [WaveformTable](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator_WaveformTable.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator.WaveformTable') where 0 represents the beginning and 1 the end of the array.  

***
[WaveformTable](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator_WaveformTable.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator.WaveformTable')

Gets or sets the current waveform table of the [WavetableOscillator](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator') of any given size.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
