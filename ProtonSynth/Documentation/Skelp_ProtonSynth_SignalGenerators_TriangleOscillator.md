#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## TriangleOscillator Class
Implementation of a triangle wave [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  
```csharp
public class TriangleOscillator : Skelp.ProtonSynth.SignalGenerators.Oscillator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') &#129106; TriangleOscillator  
### Constructors

***
[TriangleOscillator(int, double, double)](Skelp_ProtonSynth_SignalGenerators_TriangleOscillator_TriangleOscillator(int_double_double).md 'Skelp.ProtonSynth.SignalGenerators.TriangleOscillator.TriangleOscillator(int, double, double)')

Initializes a new instance of the [TriangleOscillator](Skelp_ProtonSynth_SignalGenerators_TriangleOscillator.md 'Skelp.ProtonSynth.SignalGenerators.TriangleOscillator') class.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_TriangleOscillator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.TriangleOscillator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
