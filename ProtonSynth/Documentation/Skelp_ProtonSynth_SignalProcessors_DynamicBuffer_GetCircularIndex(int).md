#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer')
## DynamicBuffer.GetCircularIndex(int) Method
Calculates an index within the range of the [DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer').  
```csharp
public int GetCircularIndex(int index);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_DynamicBuffer_GetCircularIndex(int)_index'></a>
`index` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive or negative value.
  
#### Returns
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
An index within the range of the [DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer').
