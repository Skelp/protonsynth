#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[FeedbackDelay](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay')
## FeedbackDelay.FeedbackDelay(int) Constructor
Initializes a new instance of the [FeedbackDelay](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay') class.  
```csharp
public FeedbackDelay(int sampleRate);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_FeedbackDelay_FeedbackDelay(int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate more than zero.
  
