#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter')
## Filter.SetAll(FilterType, double, double) Method
Sets the parameters and adjusts the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') accordingly.  
```csharp
public void SetAll(Skelp.ProtonSynth.SignalProcessors.FilterType filterType, double frequency, double q);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_SetAll(Skelp_ProtonSynth_SignalProcessors_FilterType_double_double)_filterType'></a>
`filterType` [FilterType](Skelp_ProtonSynth_SignalProcessors_FilterType.md 'Skelp.ProtonSynth.SignalProcessors.FilterType')  
Any [FilterType](Skelp_ProtonSynth_SignalProcessors_FilterType.md 'Skelp.ProtonSynth.SignalProcessors.FilterType').
  
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_SetAll(Skelp_ProtonSynth_SignalProcessors_FilterType_double_double)_frequency'></a>
`frequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive frequency more than zero.
  
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_SetAll(Skelp_ProtonSynth_SignalProcessors_FilterType_double_double)_q'></a>
`q` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive Q (quality factor).
  
