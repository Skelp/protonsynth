#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## SineHP Class
Implementation of "Fast and accurate sine/cosine approximation" (Michael Baczynski) by Martin Climatiano.  


  
http://www.mclimatiano.com/faster-sine-approximation-using-quadratic-curve/.  
```csharp
internal class SineHP
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; SineHP  
