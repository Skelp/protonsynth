#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend')
## PitchBend.GetFrequencyDelta() Method
Calculates the frequency difference to [Frequency](Skelp_ProtonSynth_PitchBend_Frequency.md 'Skelp.ProtonSynth.PitchBend.Frequency') based on the [SemitoneRange](Skelp_ProtonSynth_PitchBend_SemitoneRange.md 'Skelp.ProtonSynth.PitchBend.SemitoneRange') and the current [BendPosition](Skelp_ProtonSynth_PitchBend_BendPosition.md 'Skelp.ProtonSynth.PitchBend.BendPosition').  
```csharp
public double GetFrequencyDelta();
```
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
The frequency difference in Hz.
