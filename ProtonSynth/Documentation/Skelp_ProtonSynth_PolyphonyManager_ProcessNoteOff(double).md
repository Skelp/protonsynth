#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')
## PolyphonyManager.ProcessNoteOff(double) Method
Takes in any note off signal and finds the oldest [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') which is still playing at a [frequency](Skelp_ProtonSynth_PolyphonyManager_ProcessNoteOff(double).md#Skelp_ProtonSynth_PolyphonyManager_ProcessNoteOff(double)_frequency 'Skelp.ProtonSynth.PolyphonyManager.ProcessNoteOff(double).frequency').  
```csharp
public void ProcessNoteOff(double frequency);
```
#### Parameters
<a name='Skelp_ProtonSynth_PolyphonyManager_ProcessNoteOff(double)_frequency'></a>
`frequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive frequency more than zero.
  
