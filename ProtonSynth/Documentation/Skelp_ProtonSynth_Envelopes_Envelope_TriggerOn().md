#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.TriggerOn() Method
Triggers the attack phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
```csharp
public void TriggerOn();
```

Implements [TriggerOn()](Skelp_ProtonSynth_Envelopes_IEnvelope_TriggerOn().md 'Skelp.ProtonSynth.Envelopes.IEnvelope.TriggerOn()')  
