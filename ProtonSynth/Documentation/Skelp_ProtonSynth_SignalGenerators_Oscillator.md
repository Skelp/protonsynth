#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## Oscillator Class
Barebone implementation of [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  
```csharp
public abstract class Oscillator :
Skelp.ProtonSynth.SignalGenerators.IOscillator,
Skelp.ProtonSynth.SignalGenerators.ISignalGenerator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; Oscillator  

Derived  
&#8627; [FastSineOscillator](Skelp_ProtonSynth_SignalGenerators_FastSineOscillator.md 'Skelp.ProtonSynth.SignalGenerators.FastSineOscillator')  
&#8627; [PulseOscillator](Skelp_ProtonSynth_SignalGenerators_PulseOscillator.md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator')  
&#8627; [SawtoothOscillator](Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator')  
&#8627; [SineOscillator](Skelp_ProtonSynth_SignalGenerators_SineOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SineOscillator')  
&#8627; [TriangleOscillator](Skelp_ProtonSynth_SignalGenerators_TriangleOscillator.md 'Skelp.ProtonSynth.SignalGenerators.TriangleOscillator')  
&#8627; [WavetableOscillator](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator')  

Implements [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator'), [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')  
### Constructors

***
[Oscillator(int, double, double)](Skelp_ProtonSynth_SignalGenerators_Oscillator_Oscillator(int_double_double).md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Oscillator(int, double, double)')

Initializes a new instance of the [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') class.  
### Properties

***
[AmplitudeMultiplier](Skelp_ProtonSynth_SignalGenerators_Oscillator_AmplitudeMultiplier.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.AmplitudeMultiplier')

Gets or sets the maximum amplitude of the signal.  

***
[Cycle](Skelp_ProtonSynth_SignalGenerators_Oscillator_Cycle.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Cycle')

Gets or sets the current position of the [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') relative to [Frequency](Skelp_ProtonSynth_SignalGenerators_Oscillator_Frequency.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Frequency').  

***
[Frequency](Skelp_ProtonSynth_SignalGenerators_Oscillator_Frequency.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Frequency')

Gets or sets the frequency of the oscillator.  

***
[Phase](Skelp_ProtonSynth_SignalGenerators_Oscillator_Phase.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Phase')

Gets or sets the phase of the oscillator where a value of 0 represents 0 degrees and a value of 1 represents 360 degrees.  

***
[SampleRate](Skelp_ProtonSynth_SignalGenerators_Oscillator_SampleRate.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.SampleRate')

Gets the samplerate of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  
### Methods

***
[CalculateCycle()](Skelp_ProtonSynth_SignalGenerators_Oscillator_CalculateCycle().md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.CalculateCycle()')

Advances [Cycle](Skelp_ProtonSynth_SignalGenerators_Oscillator_Cycle.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Cycle') for the next sample in accordance with [Frequency](Skelp_ProtonSynth_SignalGenerators_Oscillator_Frequency.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.Frequency') and [SampleRate](Skelp_ProtonSynth_SignalGenerators_Oscillator_SampleRate.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.SampleRate').  

***
[GetAngularVelocity(double)](Skelp_ProtonSynth_SignalGenerators_Oscillator_GetAngularVelocity(double).md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.GetAngularVelocity(double)')

Calculates the angular velocity for a given [frequency](Skelp_ProtonSynth_SignalGenerators_Oscillator_GetAngularVelocity(double).md#Skelp_ProtonSynth_SignalGenerators_Oscillator_GetAngularVelocity(double)_frequency 'Skelp.ProtonSynth.SignalGenerators.Oscillator.GetAngularVelocity(double).frequency').  

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_Oscillator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  

***
[ResetCycle()](Skelp_ProtonSynth_SignalGenerators_Oscillator_ResetCycle().md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.ResetCycle()')

Resets the internal cycle of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  

***
[SetSampleRate(int)](Skelp_ProtonSynth_SignalGenerators_Oscillator_SetSampleRate(int).md 'Skelp.ProtonSynth.SignalGenerators.Oscillator.SetSampleRate(int)')

Sets the samplerate of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  
