#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')
## OverSamplingWrapper.SetOverSamplingFactor(int) Method
Updates the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') according to the new [overSamplingFactor](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetOverSamplingFactor(int).md#Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetOverSamplingFactor(int)_overSamplingFactor 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.SetOverSamplingFactor(int).overSamplingFactor').  
```csharp
public void SetOverSamplingFactor(int overSamplingFactor);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetOverSamplingFactor(int)_overSamplingFactor'></a>
`overSamplingFactor` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive factor.
  
