#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## OverSamplingWrapper Class
Wrapper class to be used with [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator') instances to automate oversampling and samplerate updates.  
```csharp
public class OverSamplingWrapper :
Skelp.ProtonSynth.SignalGenerators.ISignalGenerator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; OverSamplingWrapper  

Implements [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')  
### Constructors

***
[OverSamplingWrapper(IOscillator, int, int)](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_OverSamplingWrapper(Skelp_ProtonSynth_SignalGenerators_IOscillator_int_int).md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.OverSamplingWrapper(Skelp.ProtonSynth.SignalGenerators.IOscillator, int, int)')

Initializes a new instance of the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') class.  
### Properties

***
[AmplitudeMultiplier](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_AmplitudeMultiplier.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.AmplitudeMultiplier')

Gets or sets the maximum amplitude of the signal.  

***
[Oscillator](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.Oscillator')

Gets the [Oscillator](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.Oscillator') which is being oversampled in [GetSample()](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.GetSample()').  

***
[OutputSampleRate](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_OutputSampleRate.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.OutputSampleRate')

Gets the final samplerate the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') produces in [GetSample()](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.GetSample()').  

***
[OverSampledSampleRate](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_OverSampledSampleRate.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.OverSampledSampleRate')

Gets the internal samplerate the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') produces in [GetSample()](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.GetSample()') before being downsampled.  

***
[OverSamplingFactor](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_OverSamplingFactor.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.OverSamplingFactor')

Gets the internal oversampling factor.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.GetSample()')

Takes oversampling into consideration when asking the [Oscillator](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.Oscillator') to generate a sample.  

***
[SetOverSamplingFactor(int)](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetOverSamplingFactor(int).md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.SetOverSamplingFactor(int)')

Updates the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') according to the new [overSamplingFactor](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetOverSamplingFactor(int).md#Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetOverSamplingFactor(int)_overSamplingFactor 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.SetOverSamplingFactor(int).overSamplingFactor').  

***
[SetSampleRate(int)](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetSampleRate(int).md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.SetSampleRate(int)')

Updates the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') according to the new [sampleRate](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetSampleRate(int).md#Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_SetSampleRate(int)_sampleRate 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.SetSampleRate(int).sampleRate').  
