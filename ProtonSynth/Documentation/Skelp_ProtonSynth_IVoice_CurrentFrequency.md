#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')
## IVoice.CurrentFrequency Property
Gets the frequency that was last sent to the [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').  
```csharp
double CurrentFrequency { get; }
```
#### Property Value
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')
### Remarks
Does not reflect the actual frequency, only the original input-frequency when calling [ProcessNoteOn(double, UnitInterval)](Skelp_ProtonSynth_IVoice_ProcessNoteOn(double_Skelp_ProtonSynth_UnitInterval).md 'Skelp.ProtonSynth.IVoice.ProcessNoteOn(double, Skelp.ProtonSynth.UnitInterval)').
