#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[Waveform](Skelp_ProtonSynth_Waveform.md 'Skelp.ProtonSynth.Waveform')
## Waveform.StartPosition Property
Gets or sets the position within the cycle to start looping from.  
```csharp
public Skelp.ProtonSynth.UnitInterval StartPosition { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
