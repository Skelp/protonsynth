#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[MidiFrequencyArray](Skelp_ProtonSynth_MidiFrequencyArray.md 'Skelp.ProtonSynth.MidiFrequencyArray')
## MidiFrequencyArray.MidiFrequencyArray(double) Constructor
Initializes a new instance of the [MidiFrequencyArray](Skelp_ProtonSynth_MidiFrequencyArray.md 'Skelp.ProtonSynth.MidiFrequencyArray') class.  
```csharp
public MidiFrequencyArray(double baseFrequency=440.0);
```
#### Parameters
<a name='Skelp_ProtonSynth_MidiFrequencyArray_MidiFrequencyArray(double)_baseFrequency'></a>
`baseFrequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive frequency in Hz which represents the frequency of A4.
  
