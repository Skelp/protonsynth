#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer')
## DynamicBuffer.Resize(int) Method
Resizes the [DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer').  
```csharp
public void Resize(int capacity);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_DynamicBuffer_Resize(int)_capacity'></a>
`capacity` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive number.
  
