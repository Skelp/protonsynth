#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')
## PolyphonyManager.Reset() Method
Resets Panic mode as well as anything the [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices') offer to be reset.  
```csharp
public void Reset();
```
