#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')
## Oscillator.ResetCycle() Method
Resets the internal cycle of the [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  
```csharp
public void ResetCycle();
```

Implements [ResetCycle()](Skelp_ProtonSynth_SignalGenerators_IOscillator_ResetCycle().md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.ResetCycle()')  
