#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.SetSampleRate(int) Method
Sets the samplerate of the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope').  
```csharp
public void SetSampleRate(int sampleRate);
```
#### Parameters
<a name='Skelp_ProtonSynth_Envelopes_Envelope_SetSampleRate(int)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate.
  
