#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope')
## IEnvelope.StartAmplitude Property
Gets or sets the amplitude the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') starts with once when triggered on.  
```csharp
Skelp.ProtonSynth.UnitInterval StartAmplitude { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
