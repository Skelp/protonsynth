#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors').[Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter')
## Filter.Filter(int, FilterType, double, double) Constructor
Initializes a new instance of the [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter') class.  
```csharp
public Filter(int sampleRate=44100, Skelp.ProtonSynth.SignalProcessors.FilterType filterType=Skelp.ProtonSynth.SignalProcessors.FilterType.LowPass, double frequency=22050.0, double q=0.1);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_Filter(int_Skelp_ProtonSynth_SignalProcessors_FilterType_double_double)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate more than zero.
  
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_Filter(int_Skelp_ProtonSynth_SignalProcessors_FilterType_double_double)_filterType'></a>
`filterType` [FilterType](Skelp_ProtonSynth_SignalProcessors_FilterType.md 'Skelp.ProtonSynth.SignalProcessors.FilterType')  
Any [FilterType](Skelp_ProtonSynth_SignalProcessors_FilterType.md 'Skelp.ProtonSynth.SignalProcessors.FilterType').
  
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_Filter(int_Skelp_ProtonSynth_SignalProcessors_FilterType_double_double)_frequency'></a>
`frequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive frequency more than zero.
  
<a name='Skelp_ProtonSynth_SignalProcessors_Filter_Filter(int_Skelp_ProtonSynth_SignalProcessors_FilterType_double_double)_q'></a>
`q` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive Q (quality factor).
  
