#### [ProtonSynth](index.md 'index')
## Skelp.ProtonSynth Namespace
### Classes

***
[MidiFrequencyArray](Skelp_ProtonSynth_MidiFrequencyArray.md 'Skelp.ProtonSynth.MidiFrequencyArray')

Provides an array of frequencies for every midi note from 0 to 127.  

***
[PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend')

Implementation of a pitchbend.  

***
[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')

Represents simplified polyphony management of any number of [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') instances.  

***
[SineHP](Skelp_ProtonSynth_SineHP.md 'Skelp.ProtonSynth.SineHP')

Implementation of "Fast and accurate sine/cosine approximation" (Michael Baczynski) by Martin Climatiano.  


  
http://www.mclimatiano.com/faster-sine-approximation-using-quadratic-curve/.  

***
[ValueHandler](Skelp_ProtonSynth_ValueHandler.md 'Skelp.ProtonSynth.ValueHandler')


***
[Waveform](Skelp_ProtonSynth_Waveform.md 'Skelp.ProtonSynth.Waveform')

Container for a single cycle of a waveform.  

***
[XorShiftRandom](Skelp_ProtonSynth_XorShiftRandom.md 'Skelp.ProtonSynth.XorShiftRandom')

Generates pseudorandom primitive types with a 64-bit implementation  
of the XorShift algorithm.  
### Structs

***
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')

Represents a real number between 0 and 1.  
### Interfaces

***
[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')

Represents any constructed synthesizer voice to be used in a polymorphic synthesizer.  
