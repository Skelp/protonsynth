#### [ProtonSynth](index.md 'index')
## Skelp.ProtonSynth.Envelopes Namespace
### Classes

***
[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')

Barebone implementation of an [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[LinearEnvelope](Skelp_ProtonSynth_Envelopes_LinearEnvelope.md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope')

Implementation of a linear ADSR [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[PowerEnvelope](Skelp_ProtonSynth_Envelopes_PowerEnvelope.md 'Skelp.ProtonSynth.Envelopes.PowerEnvelope')

Implementation of an ADSR [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') based on [LinearEnvelope](Skelp_ProtonSynth_Envelopes_LinearEnvelope.md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope').  
### Interfaces

***
[IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope')

An interface that describes any signal source traditionally used as a multiplier for the maximum amplitude of any [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
