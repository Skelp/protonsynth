#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## PulseOscillator Class
Implementation of a pulse [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  
```csharp
public class PulseOscillator : Skelp.ProtonSynth.SignalGenerators.Oscillator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') &#129106; PulseOscillator  
### Constructors

***
[PulseOscillator(int, double, double, double)](Skelp_ProtonSynth_SignalGenerators_PulseOscillator_PulseOscillator(int_double_double_double).md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator.PulseOscillator(int, double, double, double)')

Initializes a new instance of the [PulseOscillator](Skelp_ProtonSynth_SignalGenerators_PulseOscillator.md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator') class.  
### Properties

***
[PulseWidth](Skelp_ProtonSynth_SignalGenerators_PulseOscillator_PulseWidth.md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator.PulseWidth')

Gets or sets the pulse width or duty cycle of the [PulseOscillator](Skelp_ProtonSynth_SignalGenerators_PulseOscillator.md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator').  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_PulseOscillator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
