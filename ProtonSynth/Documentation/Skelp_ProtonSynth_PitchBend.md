#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## PitchBend Class
Implementation of a pitchbend.  
```csharp
public class PitchBend
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; PitchBend  
### Constructors

***
[PitchBend(double, int)](Skelp_ProtonSynth_PitchBend_PitchBend(double_int).md 'Skelp.ProtonSynth.PitchBend.PitchBend(double, int)')

Initializes a new instance of the [PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend') class.  
### Properties

***
[BendPosition](Skelp_ProtonSynth_PitchBend_BendPosition.md 'Skelp.ProtonSynth.PitchBend.BendPosition')

Gets or sets the relative position of the [PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend'). 0.5 represents a neutral position.  

***
[Frequency](Skelp_ProtonSynth_PitchBend_Frequency.md 'Skelp.ProtonSynth.PitchBend.Frequency')

Gets or sets the base frequency.  

***
[SemitoneRange](Skelp_ProtonSynth_PitchBend_SemitoneRange.md 'Skelp.ProtonSynth.PitchBend.SemitoneRange')

Gets or sets the value representing the difference in semitones from [Frequency](Skelp_ProtonSynth_PitchBend_Frequency.md 'Skelp.ProtonSynth.PitchBend.Frequency') in one direction.  
### Methods

***
[GetFrequencyDelta()](Skelp_ProtonSynth_PitchBend_GetFrequencyDelta().md 'Skelp.ProtonSynth.PitchBend.GetFrequencyDelta()')

Calculates the frequency difference to [Frequency](Skelp_ProtonSynth_PitchBend_Frequency.md 'Skelp.ProtonSynth.PitchBend.Frequency') based on the [SemitoneRange](Skelp_ProtonSynth_PitchBend_SemitoneRange.md 'Skelp.ProtonSynth.PitchBend.SemitoneRange') and the current [BendPosition](Skelp_ProtonSynth_PitchBend_BendPosition.md 'Skelp.ProtonSynth.PitchBend.BendPosition').  
