#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.SustainAmplitude Property
Gets or sets the amplitude the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') maintains when [DecayTime](Skelp_ProtonSynth_Envelopes_IEnvelope_DecayTime.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.DecayTime') is over.  
```csharp
public Skelp.ProtonSynth.UnitInterval SustainAmplitude { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')

Implements [SustainAmplitude](Skelp_ProtonSynth_Envelopes_IEnvelope_SustainAmplitude.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.SustainAmplitude')  
