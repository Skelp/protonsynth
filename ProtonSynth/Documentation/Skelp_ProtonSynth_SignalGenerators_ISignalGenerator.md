#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## ISignalGenerator Interface
An interface that describes any signal source.  
```csharp
public interface ISignalGenerator
```

Derived  
&#8627; [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator')  
&#8627; [NoiseGenerator](Skelp_ProtonSynth_SignalGenerators_NoiseGenerator.md 'Skelp.ProtonSynth.SignalGenerators.NoiseGenerator')  
&#8627; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')  
&#8627; [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')  
&#8627; [SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer')  
### Properties

***
[AmplitudeMultiplier](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator_AmplitudeMultiplier.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator.AmplitudeMultiplier')

Gets or sets the maximum amplitude of the signal.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
