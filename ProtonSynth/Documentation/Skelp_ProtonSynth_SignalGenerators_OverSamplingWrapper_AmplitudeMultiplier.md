#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')
## OverSamplingWrapper.AmplitudeMultiplier Property
Gets or sets the maximum amplitude of the signal.  
```csharp
public Skelp.ProtonSynth.UnitInterval AmplitudeMultiplier { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')

Implements [AmplitudeMultiplier](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator_AmplitudeMultiplier.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator.AmplitudeMultiplier')  
