#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors')
## DynamicBuffer Class
Represents a buffer with dynamic sizing. Can be resized any time.  
```csharp
public class DynamicBuffer : System.Collections.Generic.List<double>
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [System.Collections.Generic.List&lt;](https://docs.microsoft.com/en-us/dotnet/api/System.Collections.Generic.List-1 'System.Collections.Generic.List`1')[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')[&gt;](https://docs.microsoft.com/en-us/dotnet/api/System.Collections.Generic.List-1 'System.Collections.Generic.List`1') &#129106; DynamicBuffer  
### Constructors

***
[DynamicBuffer(int)](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer_DynamicBuffer(int).md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer.DynamicBuffer(int)')

Initializes a new instance of the [DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer') class.  
### Methods

***
[GetCircularIndex(int)](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer_GetCircularIndex(int).md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer.GetCircularIndex(int)')

Calculates an index within the range of the [DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer').  

***
[Resize(int)](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer_Resize(int).md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer.Resize(int)')

Resizes the [DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer').  
