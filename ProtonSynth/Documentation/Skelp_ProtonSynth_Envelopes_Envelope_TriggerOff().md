#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.TriggerOff() Method
Triggers the release phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
```csharp
public void TriggerOff();
```

Implements [TriggerOff()](Skelp_ProtonSynth_Envelopes_IEnvelope_TriggerOff().md 'Skelp.ProtonSynth.Envelopes.IEnvelope.TriggerOff()')  
