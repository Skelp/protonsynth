#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## Waveform Class
Container for a single cycle of a waveform.  
```csharp
public class Waveform
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; Waveform  
### Properties

***
[EndPosition](Skelp_ProtonSynth_Waveform_EndPosition.md 'Skelp.ProtonSynth.Waveform.EndPosition')

Gets or sets the last position within the cycle before going back to [StartPosition](Skelp_ProtonSynth_Waveform_StartPosition.md 'Skelp.ProtonSynth.Waveform.StartPosition').  

***
[Samples](Skelp_ProtonSynth_Waveform_Samples.md 'Skelp.ProtonSynth.Waveform.Samples')

Gets or sets the waveform data of any given length.  

***
[StartPosition](Skelp_ProtonSynth_Waveform_StartPosition.md 'Skelp.ProtonSynth.Waveform.StartPosition')

Gets or sets the position within the cycle to start looping from.  
