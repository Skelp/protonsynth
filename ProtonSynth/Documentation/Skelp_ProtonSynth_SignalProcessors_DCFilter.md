#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalProcessors](Skelp_ProtonSynth_SignalProcessors.md 'Skelp.ProtonSynth.SignalProcessors')
## DCFilter Class
Filters out low frequencies and DC offsets gently.  
```csharp
public class DCFilter :
Skelp.ProtonSynth.SignalProcessors.ISignalProcessor
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; DCFilter  

Implements [ISignalProcessor](Skelp_ProtonSynth_SignalProcessors_ISignalProcessor.md 'Skelp.ProtonSynth.SignalProcessors.ISignalProcessor')  
### Remarks
Advisable to use as the last processor in the chain.
### Constructors

***
[DCFilter(int)](Skelp_ProtonSynth_SignalProcessors_DCFilter_DCFilter(int).md 'Skelp.ProtonSynth.SignalProcessors.DCFilter.DCFilter(int)')

Initializes a new instance of the [DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter') class.  
### Properties

***
[SampleRate](Skelp_ProtonSynth_SignalProcessors_DCFilter_SampleRate.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter.SampleRate')

Gets the samplerate of the [DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter').  
### Methods

***
[ProcessSample(double)](Skelp_ProtonSynth_SignalProcessors_DCFilter_ProcessSample(double).md 'Skelp.ProtonSynth.SignalProcessors.DCFilter.ProcessSample(double)')

Processes the given [sample](Skelp_ProtonSynth_SignalProcessors_DCFilter_ProcessSample(double).md#Skelp_ProtonSynth_SignalProcessors_DCFilter_ProcessSample(double)_sample 'Skelp.ProtonSynth.SignalProcessors.DCFilter.ProcessSample(double).sample').  

***
[SetSampleRate(int)](Skelp_ProtonSynth_SignalProcessors_DCFilter_SetSampleRate(int).md 'Skelp.ProtonSynth.SignalProcessors.DCFilter.SetSampleRate(int)')

Sets the samplerate of the [DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter').  
