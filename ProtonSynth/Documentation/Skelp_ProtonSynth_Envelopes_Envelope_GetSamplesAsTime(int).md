#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.GetSamplesAsTime(int) Method
Converts samples to time based on [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').  
```csharp
private protected System.TimeSpan GetSamplesAsTime(int samples);
```
#### Parameters
<a name='Skelp_ProtonSynth_Envelopes_Envelope_GetSamplesAsTime(int)_samples'></a>
`samples` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any time in samples.
  
#### Returns
[System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan')  
A [System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan') with a length of [samples](Skelp_ProtonSynth_Envelopes_Envelope_GetSamplesAsTime(int).md#Skelp_ProtonSynth_Envelopes_Envelope_GetSamplesAsTime(int)_samples 'Skelp.ProtonSynth.Envelopes.Envelope.GetSamplesAsTime(int).samples') relative to [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').
