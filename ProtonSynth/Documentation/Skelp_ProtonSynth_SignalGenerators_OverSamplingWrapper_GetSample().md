#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')
## OverSamplingWrapper.GetSample() Method
Takes oversampling into consideration when asking the [Oscillator](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.Oscillator') to generate a sample.  
```csharp
public double GetSample();
```
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
An oversampled sample of [Oscillator](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.Oscillator') in accordance to the wrapper state.

Implements [GetSample()](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator.GetSample()')  
