#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[SawtoothOscillator](Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator')
## SawtoothOscillator.SawtoothOscillator(int, double, double) Constructor
Initializes a new instance of the [SawtoothOscillator](Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator') class.  
```csharp
public SawtoothOscillator(int sampleRate, double frequency=440.0, double amplitudeMultiplier=1.0);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator_SawtoothOscillator(int_double_double)_sampleRate'></a>
`sampleRate` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
Any positive samplerate more than zero.
  
<a name='Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator_SawtoothOscillator(int_double_double)_frequency'></a>
`frequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive frequency more than zero.
  
<a name='Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator_SawtoothOscillator(int_double_double)_amplitudeMultiplier'></a>
`amplitudeMultiplier` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any value between [MinValue](Skelp_ProtonSynth_UnitInterval_MinValue.md 'Skelp.ProtonSynth.UnitInterval.MinValue') and [MaxValue](Skelp_ProtonSynth_UnitInterval_MaxValue.md 'Skelp.ProtonSynth.UnitInterval.MaxValue').
  
