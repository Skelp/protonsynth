#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes')
## Envelope Class
Barebone implementation of an [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
```csharp
public abstract class Envelope :
Skelp.ProtonSynth.Envelopes.IEnvelope
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; Envelope  

Derived  
&#8627; [LinearEnvelope](Skelp_ProtonSynth_Envelopes_LinearEnvelope.md 'Skelp.ProtonSynth.Envelopes.LinearEnvelope')  

Implements [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope')  
### Constructors

***
[Envelope(int)](Skelp_ProtonSynth_Envelopes_Envelope_Envelope(int).md 'Skelp.ProtonSynth.Envelopes.Envelope.Envelope(int)')

Initializes a new instance of the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope') class.  

***
[Envelope(TimeSpan, TimeSpan, UnitInterval, TimeSpan, int)](Skelp_ProtonSynth_Envelopes_Envelope_Envelope(System_TimeSpan_System_TimeSpan_Skelp_ProtonSynth_UnitInterval_System_TimeSpan_int).md 'Skelp.ProtonSynth.Envelopes.Envelope.Envelope(System.TimeSpan, System.TimeSpan, Skelp.ProtonSynth.UnitInterval, System.TimeSpan, int)')

Initializes a new instance of the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope') class.  
### Properties

***
[AmplitudeMultiplier](Skelp_ProtonSynth_Envelopes_Envelope_AmplitudeMultiplier.md 'Skelp.ProtonSynth.Envelopes.Envelope.AmplitudeMultiplier')

Gets or sets the maximum value of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[AttackTime](Skelp_ProtonSynth_Envelopes_Envelope_AttackTime.md 'Skelp.ProtonSynth.Envelopes.Envelope.AttackTime')

Gets or sets the time spent in the attack phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[AttackTimeInSamples](Skelp_ProtonSynth_Envelopes_Envelope_AttackTimeInSamples.md 'Skelp.ProtonSynth.Envelopes.Envelope.AttackTimeInSamples')

Gets or sets the attack time in samples based on [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').  

***
[DecayTime](Skelp_ProtonSynth_Envelopes_Envelope_DecayTime.md 'Skelp.ProtonSynth.Envelopes.Envelope.DecayTime')

Gets or sets the time spent in the decay phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[DecayTimeInSamples](Skelp_ProtonSynth_Envelopes_Envelope_DecayTimeInSamples.md 'Skelp.ProtonSynth.Envelopes.Envelope.DecayTimeInSamples')

Gets or sets the decay time in samples based on [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').  

***
[EnvelopeIsTriggeredOn](Skelp_ProtonSynth_Envelopes_Envelope_EnvelopeIsTriggeredOn.md 'Skelp.ProtonSynth.Envelopes.Envelope.EnvelopeIsTriggeredOn')

Gets or sets a value indicating whether or not the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope') is triggered on.  

***
[LastAmplitude](Skelp_ProtonSynth_Envelopes_Envelope_LastAmplitude.md 'Skelp.ProtonSynth.Envelopes.Envelope.LastAmplitude')

Gets or sets the value of the most recent amplitude.  

***
[ReleaseTime](Skelp_ProtonSynth_Envelopes_Envelope_ReleaseTime.md 'Skelp.ProtonSynth.Envelopes.Envelope.ReleaseTime')

Gets or sets the time spent in the release phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[ReleaseTimeInSamples](Skelp_ProtonSynth_Envelopes_Envelope_ReleaseTimeInSamples.md 'Skelp.ProtonSynth.Envelopes.Envelope.ReleaseTimeInSamples')

Gets or sets the release time in samples based on [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').  

***
[SampleCounter](Skelp_ProtonSynth_Envelopes_Envelope_SampleCounter.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleCounter')

Gets or sets the most recent position within the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope') relative to its trigger state.  

***
[SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate')

Gets the samplerate of the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope').  

***
[StartAmplitude](Skelp_ProtonSynth_Envelopes_Envelope_StartAmplitude.md 'Skelp.ProtonSynth.Envelopes.Envelope.StartAmplitude')

Gets or sets the amplitude the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') starts with once when triggered on.  

***
[SustainAmplitude](Skelp_ProtonSynth_Envelopes_Envelope_SustainAmplitude.md 'Skelp.ProtonSynth.Envelopes.Envelope.SustainAmplitude')

Gets or sets the amplitude the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') maintains when [DecayTime](Skelp_ProtonSynth_Envelopes_IEnvelope_DecayTime.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.DecayTime') is over.  
### Methods

***
[GetAmplitude()](Skelp_ProtonSynth_Envelopes_Envelope_GetAmplitude().md 'Skelp.ProtonSynth.Envelopes.Envelope.GetAmplitude()')

Gets the next sample according to the inner state of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[GetSamplesAsTime(int)](Skelp_ProtonSynth_Envelopes_Envelope_GetSamplesAsTime(int).md 'Skelp.ProtonSynth.Envelopes.Envelope.GetSamplesAsTime(int)')

Converts samples to time based on [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').  

***
[GetTimeInSamples(TimeSpan)](Skelp_ProtonSynth_Envelopes_Envelope_GetTimeInSamples(System_TimeSpan).md 'Skelp.ProtonSynth.Envelopes.Envelope.GetTimeInSamples(System.TimeSpan)')

Converts time to samples based on [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').  

***
[SetSampleRate(int)](Skelp_ProtonSynth_Envelopes_Envelope_SetSampleRate(int).md 'Skelp.ProtonSynth.Envelopes.Envelope.SetSampleRate(int)')

Sets the samplerate of the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope').  

***
[TriggerOff()](Skelp_ProtonSynth_Envelopes_Envelope_TriggerOff().md 'Skelp.ProtonSynth.Envelopes.Envelope.TriggerOff()')

Triggers the release phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[TriggerOn()](Skelp_ProtonSynth_Envelopes_Envelope_TriggerOn().md 'Skelp.ProtonSynth.Envelopes.Envelope.TriggerOn()')

Triggers the attack phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
