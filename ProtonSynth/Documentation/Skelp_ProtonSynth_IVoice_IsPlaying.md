#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')
## IVoice.IsPlaying Property
Gets a value indicating whether [ProcessNoteOn(double, UnitInterval)](Skelp_ProtonSynth_IVoice_ProcessNoteOn(double_Skelp_ProtonSynth_UnitInterval).md 'Skelp.ProtonSynth.IVoice.ProcessNoteOn(double, Skelp.ProtonSynth.UnitInterval)') or [ProcessNoteOff()](Skelp_ProtonSynth_IVoice_ProcessNoteOff().md 'Skelp.ProtonSynth.IVoice.ProcessNoteOff()') was last called.  
```csharp
bool IsPlaying { get; }
```
#### Property Value
[System.Boolean](https://docs.microsoft.com/en-us/dotnet/api/System.Boolean 'System.Boolean')
