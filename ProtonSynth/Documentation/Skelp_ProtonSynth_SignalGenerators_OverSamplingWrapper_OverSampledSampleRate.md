#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')
## OverSamplingWrapper.OverSampledSampleRate Property
Gets the internal samplerate the [OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper') produces in [GetSample()](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper.GetSample()') before being downsampled.  
```csharp
public int OverSampledSampleRate { get; set; }
```
#### Property Value
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')
