#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth')
## XorShiftRandom Class
Generates pseudorandom primitive types with a 64-bit implementation  
of the XorShift algorithm.  
```csharp
internal class XorShiftRandom
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; XorShiftRandom  
### Constructors

***
[XorShiftRandom()](Skelp_ProtonSynth_XorShiftRandom_XorShiftRandom().md 'Skelp.ProtonSynth.XorShiftRandom.XorShiftRandom()')

Constructs a new  generator using two  
random Guid hash codes as a seed.  

***
[XorShiftRandom(ulong)](Skelp_ProtonSynth_XorShiftRandom_XorShiftRandom(ulong).md 'Skelp.ProtonSynth.XorShiftRandom.XorShiftRandom(ulong)')

Constructs a new  generator  
with the supplied seed.  
### Methods

***
[NextDouble()](Skelp_ProtonSynth_XorShiftRandom_NextDouble().md 'Skelp.ProtonSynth.XorShiftRandom.NextDouble()')

Generates a pseudorandom double between  
0 and 1 non-inclusive.  
