#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[Waveform](Skelp_ProtonSynth_Waveform.md 'Skelp.ProtonSynth.Waveform')
## Waveform.Samples Property
Gets or sets the waveform data of any given length.  
```csharp
public double[] Samples { get; set; }
```
#### Property Value
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')[[]](https://docs.microsoft.com/en-us/dotnet/api/System.Array 'System.Array')
