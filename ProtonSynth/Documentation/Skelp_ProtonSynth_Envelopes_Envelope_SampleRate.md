#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.SampleRate Property
Gets the samplerate of the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope').  
```csharp
public int SampleRate { get; set; }
```
#### Property Value
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')

Implements [SampleRate](Skelp_ProtonSynth_Envelopes_IEnvelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.SampleRate')  
