#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[XorShiftRandom](Skelp_ProtonSynth_XorShiftRandom.md 'Skelp.ProtonSynth.XorShiftRandom')
## XorShiftRandom.XorShiftRandom(ulong) Constructor
Constructs a new  generator  
with the supplied seed.  
```csharp
public XorShiftRandom(ulong seed);
```
#### Parameters
<a name='Skelp_ProtonSynth_XorShiftRandom_XorShiftRandom(ulong)_seed'></a>
`seed` [System.UInt64](https://docs.microsoft.com/en-us/dotnet/api/System.UInt64 'System.UInt64')  
The seed value.  
  
