#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.GetTimeInSamples(TimeSpan) Method
Converts time to samples based on [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').  
```csharp
private protected int GetTimeInSamples(System.TimeSpan timeSpan);
```
#### Parameters
<a name='Skelp_ProtonSynth_Envelopes_Envelope_GetTimeInSamples(System_TimeSpan)_timeSpan'></a>
`timeSpan` [System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan')  
Any [System.TimeSpan](https://docs.microsoft.com/en-us/dotnet/api/System.TimeSpan 'System.TimeSpan').
  
#### Returns
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
An [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32') representing time in samples relative to [SampleRate](Skelp_ProtonSynth_Envelopes_Envelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.Envelope.SampleRate').
