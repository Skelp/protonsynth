#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')
## PolyphonyManager.ProcessPitchBend(UnitInterval) Method
Processes pitchbend signals and forwards them to all [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice') instances.  
```csharp
public void ProcessPitchBend(Skelp.ProtonSynth.UnitInterval bendPosition);
```
#### Parameters
<a name='Skelp_ProtonSynth_PolyphonyManager_ProcessPitchBend(Skelp_ProtonSynth_UnitInterval)_bendPosition'></a>
`bendPosition` [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')  
Any valid [UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval'). Value 0.5 represents the middle of the [PitchBend](Skelp_ProtonSynth_PitchBend.md 'Skelp.ProtonSynth.PitchBend').
  
