#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## NoiseGenerator Class
Implementation of a pseudo random noise [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
```csharp
public class NoiseGenerator :
Skelp.ProtonSynth.SignalGenerators.ISignalGenerator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; NoiseGenerator  

Implements [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')  
### Properties

***
[AmplitudeMultiplier](Skelp_ProtonSynth_SignalGenerators_NoiseGenerator_AmplitudeMultiplier.md 'Skelp.ProtonSynth.SignalGenerators.NoiseGenerator.AmplitudeMultiplier')

Gets or sets the maximum amplitude of the signal.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_NoiseGenerator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.NoiseGenerator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
