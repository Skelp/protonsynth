#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## SignalMixer Class
Helper class to mix signal sources together whilst staying within the sample bounds of -1 and +1.  
```csharp
public class SignalMixer :
Skelp.ProtonSynth.SignalGenerators.ISignalGenerator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; SignalMixer  

Implements [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')  
### Constructors

***
[SignalMixer(ISignalGenerator[])](Skelp_ProtonSynth_SignalGenerators_SignalMixer_SignalMixer(Skelp_ProtonSynth_SignalGenerators_ISignalGenerator__).md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer.SignalMixer(Skelp.ProtonSynth.SignalGenerators.ISignalGenerator[])')

Initializes a new instance of the [SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer') class.  

***
[SignalMixer(IEnumerable&lt;ISignalGenerator&gt;)](Skelp_ProtonSynth_SignalGenerators_SignalMixer_SignalMixer(System_Collections_Generic_IEnumerable_Skelp_ProtonSynth_SignalGenerators_ISignalGenerator_).md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer.SignalMixer(System.Collections.Generic.IEnumerable&lt;Skelp.ProtonSynth.SignalGenerators.ISignalGenerator&gt;)')

Initializes a new instance of the [SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer') class.  
### Properties

***
[AmplitudeMultiplier](Skelp_ProtonSynth_SignalGenerators_SignalMixer_AmplitudeMultiplier.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer.AmplitudeMultiplier')

Gets or sets the maximum amplitude of the signal.  

***
[SignalGenerators](Skelp_ProtonSynth_SignalGenerators_SignalMixer_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer.SignalGenerators')

Gets the collection of [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator') instances of the [SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer').  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_SignalMixer_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
