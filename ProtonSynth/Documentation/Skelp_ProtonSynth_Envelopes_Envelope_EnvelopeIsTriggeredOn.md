#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')
## Envelope.EnvelopeIsTriggeredOn Property
Gets or sets a value indicating whether or not the [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope') is triggered on.  
```csharp
private protected bool EnvelopeIsTriggeredOn { get; set; }
```
#### Property Value
[System.Boolean](https://docs.microsoft.com/en-us/dotnet/api/System.Boolean 'System.Boolean')
