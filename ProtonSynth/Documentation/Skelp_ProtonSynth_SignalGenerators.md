#### [ProtonSynth](index.md 'index')
## Skelp.ProtonSynth.SignalGenerators Namespace
### Classes

***
[FastSineOscillator](Skelp_ProtonSynth_SignalGenerators_FastSineOscillator.md 'Skelp.ProtonSynth.SignalGenerators.FastSineOscillator')

Implementation of an approximation of a sine [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  


  
Reduces computation time but introduces harmonics.  

***
[NoiseGenerator](Skelp_ProtonSynth_SignalGenerators_NoiseGenerator.md 'Skelp.ProtonSynth.SignalGenerators.NoiseGenerator')

Implementation of a pseudo random noise [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  

***
[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')

Barebone implementation of [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator').  

***
[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')

Wrapper class to be used with [IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator') instances to automate oversampling and samplerate updates.  

***
[PulseOscillator](Skelp_ProtonSynth_SignalGenerators_PulseOscillator.md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator')

Implementation of a pulse [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  

***
[RampOscillator](Skelp_ProtonSynth_SignalGenerators_RampOscillator.md 'Skelp.ProtonSynth.SignalGenerators.RampOscillator')

Implementation of a ramp [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  

***
[SawtoothOscillator](Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator')

Implementation of a sawtooth [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  

***
[SignalMixer](Skelp_ProtonSynth_SignalGenerators_SignalMixer.md 'Skelp.ProtonSynth.SignalGenerators.SignalMixer')

Helper class to mix signal sources together whilst staying within the sample bounds of -1 and +1.  

***
[SineOscillator](Skelp_ProtonSynth_SignalGenerators_SineOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SineOscillator')

Implementation of a sine [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  

***
[TriangleOscillator](Skelp_ProtonSynth_SignalGenerators_TriangleOscillator.md 'Skelp.ProtonSynth.SignalGenerators.TriangleOscillator')

Implementation of a triangle wave [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  

***
[WavetableOscillator](Skelp_ProtonSynth_SignalGenerators_WavetableOscillator.md 'Skelp.ProtonSynth.SignalGenerators.WavetableOscillator')

Implementation of a wavetable [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  
### Interfaces

***
[IOscillator](Skelp_ProtonSynth_SignalGenerators_IOscillator.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator')

An interface that describes any oscillating signal source.  

***
[ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator')

An interface that describes any signal source.  
