#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[PolyphonyManager](Skelp_ProtonSynth_PolyphonyManager.md 'Skelp.ProtonSynth.PolyphonyManager')
## PolyphonyManager.GetSamples(int) Method
Processes all [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices') in parallel by calling their respective [GetSample()](Skelp_ProtonSynth_IVoice_GetSample().md 'Skelp.ProtonSynth.IVoice.GetSample()').  
```csharp
public double[] GetSamples(int amount);
```
#### Parameters
<a name='Skelp_ProtonSynth_PolyphonyManager_GetSamples(int)_amount'></a>
`amount` [System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')  
  
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')[[]](https://docs.microsoft.com/en-us/dotnet/api/System.Array 'System.Array')  
The summed samples of all [Voices](Skelp_ProtonSynth_PolyphonyManager_Voices.md 'Skelp.ProtonSynth.PolyphonyManager.Voices') multiplied by [Amplitude](Skelp_ProtonSynth_PolyphonyManager_Amplitude.md 'Skelp.ProtonSynth.PolyphonyManager.Amplitude').
### Remarks
.NET Standard 1.0 does not support the Parallel library and will run this method sequentially instead.
