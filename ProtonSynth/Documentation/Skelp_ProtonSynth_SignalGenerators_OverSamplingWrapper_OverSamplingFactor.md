#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[OverSamplingWrapper](Skelp_ProtonSynth_SignalGenerators_OverSamplingWrapper.md 'Skelp.ProtonSynth.SignalGenerators.OverSamplingWrapper')
## OverSamplingWrapper.OverSamplingFactor Property
Gets the internal oversampling factor.  
```csharp
public int OverSamplingFactor { get; set; }
```
#### Property Value
[System.Int32](https://docs.microsoft.com/en-us/dotnet/api/System.Int32 'System.Int32')
