#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth](Skelp_ProtonSynth.md 'Skelp.ProtonSynth').[IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice')
## IVoice.ProcessNoteOff() Method
Processes the note off signal by setting the state of relevant members in [IVoice](Skelp_ProtonSynth_IVoice.md 'Skelp.ProtonSynth.IVoice').  
```csharp
void ProcessNoteOff();
```
