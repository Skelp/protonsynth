#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')
## Oscillator.Frequency Property
Gets or sets the frequency of the oscillator.  
```csharp
public double Frequency { get; set; }
```
#### Property Value
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')

Implements [Frequency](Skelp_ProtonSynth_SignalGenerators_IOscillator_Frequency.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.Frequency')  
