#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators')
## RampOscillator Class
Implementation of a ramp [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator').  
```csharp
public class RampOscillator : Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator
```

Inheritance [System.Object](https://docs.microsoft.com/en-us/dotnet/api/System.Object 'System.Object') &#129106; [Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator') &#129106; [SawtoothOscillator](Skelp_ProtonSynth_SignalGenerators_SawtoothOscillator.md 'Skelp.ProtonSynth.SignalGenerators.SawtoothOscillator') &#129106; RampOscillator  
### Constructors

***
[RampOscillator(int, double, double)](Skelp_ProtonSynth_SignalGenerators_RampOscillator_RampOscillator(int_double_double).md 'Skelp.ProtonSynth.SignalGenerators.RampOscillator.RampOscillator(int, double, double)')

Initializes a new instance of the [RampOscillator](Skelp_ProtonSynth_SignalGenerators_RampOscillator.md 'Skelp.ProtonSynth.SignalGenerators.RampOscillator') class.  
### Methods

***
[GetSample()](Skelp_ProtonSynth_SignalGenerators_RampOscillator_GetSample().md 'Skelp.ProtonSynth.SignalGenerators.RampOscillator.GetSample()')

Gets the next sample according to the inner state of the [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
