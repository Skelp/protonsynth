#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes')
## IEnvelope Interface
An interface that describes any signal source traditionally used as a multiplier for the maximum amplitude of any [ISignalGenerator](Skelp_ProtonSynth_SignalGenerators_ISignalGenerator.md 'Skelp.ProtonSynth.SignalGenerators.ISignalGenerator').  
```csharp
public interface IEnvelope
```

Derived  
&#8627; [Envelope](Skelp_ProtonSynth_Envelopes_Envelope.md 'Skelp.ProtonSynth.Envelopes.Envelope')  
### Properties

***
[AmplitudeMultiplier](Skelp_ProtonSynth_Envelopes_IEnvelope_AmplitudeMultiplier.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.AmplitudeMultiplier')

Gets or sets the maximum value of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[AttackTime](Skelp_ProtonSynth_Envelopes_IEnvelope_AttackTime.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.AttackTime')

Gets or sets the time spent in the attack phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[DecayTime](Skelp_ProtonSynth_Envelopes_IEnvelope_DecayTime.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.DecayTime')

Gets or sets the time spent in the decay phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[ReleaseTime](Skelp_ProtonSynth_Envelopes_IEnvelope_ReleaseTime.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.ReleaseTime')

Gets or sets the time spent in the release phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[SampleRate](Skelp_ProtonSynth_Envelopes_IEnvelope_SampleRate.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.SampleRate')

Gets or sets the samplerate of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[StartAmplitude](Skelp_ProtonSynth_Envelopes_IEnvelope_StartAmplitude.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.StartAmplitude')

Gets or sets the amplitude the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') starts with once when triggered on.  

***
[SustainAmplitude](Skelp_ProtonSynth_Envelopes_IEnvelope_SustainAmplitude.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.SustainAmplitude')

Gets or sets the amplitude the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope') maintains when [DecayTime](Skelp_ProtonSynth_Envelopes_IEnvelope_DecayTime.md 'Skelp.ProtonSynth.Envelopes.IEnvelope.DecayTime') is over.  
### Methods

***
[GetAmplitude()](Skelp_ProtonSynth_Envelopes_IEnvelope_GetAmplitude().md 'Skelp.ProtonSynth.Envelopes.IEnvelope.GetAmplitude()')

Gets the next sample according to the inner state of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[TriggerOff()](Skelp_ProtonSynth_Envelopes_IEnvelope_TriggerOff().md 'Skelp.ProtonSynth.Envelopes.IEnvelope.TriggerOff()')

Triggers the release phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  

***
[TriggerOn()](Skelp_ProtonSynth_Envelopes_IEnvelope_TriggerOn().md 'Skelp.ProtonSynth.Envelopes.IEnvelope.TriggerOn()')

Triggers the attack phase of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
