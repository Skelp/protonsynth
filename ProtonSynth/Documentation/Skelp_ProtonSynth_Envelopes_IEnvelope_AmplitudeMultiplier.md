#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.Envelopes](Skelp_ProtonSynth_Envelopes.md 'Skelp.ProtonSynth.Envelopes').[IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope')
## IEnvelope.AmplitudeMultiplier Property
Gets or sets the maximum value of the [IEnvelope](Skelp_ProtonSynth_Envelopes_IEnvelope.md 'Skelp.ProtonSynth.Envelopes.IEnvelope').  
```csharp
Skelp.ProtonSynth.UnitInterval AmplitudeMultiplier { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
