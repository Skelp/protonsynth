#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[PulseOscillator](Skelp_ProtonSynth_SignalGenerators_PulseOscillator.md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator')
## PulseOscillator.PulseWidth Property
Gets or sets the pulse width or duty cycle of the [PulseOscillator](Skelp_ProtonSynth_SignalGenerators_PulseOscillator.md 'Skelp.ProtonSynth.SignalGenerators.PulseOscillator').  
```csharp
public Skelp.ProtonSynth.UnitInterval PulseWidth { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')
