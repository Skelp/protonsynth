#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')
## Oscillator.Phase Property
Gets or sets the phase of the oscillator where a value of 0 represents 0 degrees and a value of 1 represents 360 degrees.  
```csharp
public Skelp.ProtonSynth.UnitInterval Phase { get; set; }
```
#### Property Value
[UnitInterval](Skelp_ProtonSynth_UnitInterval.md 'Skelp.ProtonSynth.UnitInterval')

Implements [Phase](Skelp_ProtonSynth_SignalGenerators_IOscillator_Phase.md 'Skelp.ProtonSynth.SignalGenerators.IOscillator.Phase')  
