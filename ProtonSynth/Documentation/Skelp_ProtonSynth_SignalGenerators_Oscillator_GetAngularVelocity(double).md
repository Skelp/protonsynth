#### [ProtonSynth](index.md 'index')
### [Skelp.ProtonSynth.SignalGenerators](Skelp_ProtonSynth_SignalGenerators.md 'Skelp.ProtonSynth.SignalGenerators').[Oscillator](Skelp_ProtonSynth_SignalGenerators_Oscillator.md 'Skelp.ProtonSynth.SignalGenerators.Oscillator')
## Oscillator.GetAngularVelocity(double) Method
Calculates the angular velocity for a given [frequency](Skelp_ProtonSynth_SignalGenerators_Oscillator_GetAngularVelocity(double).md#Skelp_ProtonSynth_SignalGenerators_Oscillator_GetAngularVelocity(double)_frequency 'Skelp.ProtonSynth.SignalGenerators.Oscillator.GetAngularVelocity(double).frequency').  
```csharp
private protected double GetAngularVelocity(double frequency);
```
#### Parameters
<a name='Skelp_ProtonSynth_SignalGenerators_Oscillator_GetAngularVelocity(double)_frequency'></a>
`frequency` [System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
Any positive frequency more than zero.
  
#### Returns
[System.Double](https://docs.microsoft.com/en-us/dotnet/api/System.Double 'System.Double')  
The angular velocity of [frequency](Skelp_ProtonSynth_SignalGenerators_Oscillator_GetAngularVelocity(double).md#Skelp_ProtonSynth_SignalGenerators_Oscillator_GetAngularVelocity(double)_frequency 'Skelp.ProtonSynth.SignalGenerators.Oscillator.GetAngularVelocity(double).frequency').
