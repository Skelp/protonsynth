#### [ProtonSynth](index.md 'index')
## Skelp.ProtonSynth.SignalProcessors Namespace
### Classes

***
[DCFilter](Skelp_ProtonSynth_SignalProcessors_DCFilter.md 'Skelp.ProtonSynth.SignalProcessors.DCFilter')

Filters out low frequencies and DC offsets gently.  

***
[DynamicBuffer](Skelp_ProtonSynth_SignalProcessors_DynamicBuffer.md 'Skelp.ProtonSynth.SignalProcessors.DynamicBuffer')

Represents a buffer with dynamic sizing. Can be resized any time.  

***
[FeedbackDelay](Skelp_ProtonSynth_SignalProcessors_FeedbackDelay.md 'Skelp.ProtonSynth.SignalProcessors.FeedbackDelay')

Provides a simple delay with feedback functionality.  

***
[Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter')

Filter based off of the original C code on  which implements a digital filter as seen in the RBJ Audio-EQ-Cookbook.  
### Interfaces

***
[ISignalProcessor](Skelp_ProtonSynth_SignalProcessors_ISignalProcessor.md 'Skelp.ProtonSynth.SignalProcessors.ISignalProcessor')

An interface that describes any signal altering unit.  
### Enums

***
[FilterType](Skelp_ProtonSynth_SignalProcessors_FilterType.md 'Skelp.ProtonSynth.SignalProcessors.FilterType')

Identifies and controls the type of [Filter](Skelp_ProtonSynth_SignalProcessors_Filter.md 'Skelp.ProtonSynth.SignalProcessors.Filter').  
