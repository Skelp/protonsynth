﻿using System;

namespace Skelp.ProtonSynth.SignalProcessors
{
    /// <summary>
    /// Identifies and controls the type of <see cref="Filter"/>.
    /// </summary>
    public enum FilterType
    {
        /// <summary>
        /// Represents a lowpass filter.
        /// </summary>
        LowPass,

        /// <summary>
        /// Represents a highpass filter.
        /// </summary>
        HighPass
    }

    /// <summary>
    /// Filter based off of the original C code on <see>http://aikelab.net/filter/</see> which implements a digital filter as seen in the RBJ Audio-EQ-Cookbook.
    /// </summary>
    public class Filter : ISignalProcessor
    {
        private double b0a0;
        private double b1a0;
        private double b2a0;
        private double a1a0;
        private double a2a0;
        private double x1;
        private double y1;
        private double x2;
        private double y2;
        private FilterType currentFilterType;
        private double currentF0;
        private double currentQ;

        /// <summary>
        /// Initializes a new instance of the <see cref="Filter"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="filterType">Any <see cref="FilterType"/>.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="q">Any positive Q (quality factor).</param>
        public Filter(int sampleRate = 44100, FilterType filterType = FilterType.LowPass, double frequency = 22050, double q = 0.1)
        {
            if (sampleRate <= 0)
            {
                throw new ArgumentException($"{nameof(sampleRate)} must be a positive non-zero value.");
            }

            this.SampleRate = sampleRate;
            SetAll(filterType, frequency, q);
        }

        /// <summary>
        /// Gets the samplerate of the <see cref="Filter"/>.
        /// </summary>
        public int SampleRate { get; private protected set; }

        /// <inheritdoc/>
        public double ProcessSample(double sample)
        {
            double result = (b0a0 * sample) + (b1a0 * x1) + (b2a0 * x2) - (a1a0 * y1) - (a2a0 * y2);

            x2 = x1;
            x1 = sample;
            y2 = y1;
            y1 = result;

            return result;
        }

        /// <summary>
        /// Sets the current Q and adjusts the <see cref="Filter"/> accordingly.
        /// </summary>
        /// <param name="q">Any positive Q (quality factor).</param>
        public void SetQ(double q)
        {
            if (q < 0)
            {
                throw new ArgumentException($"{nameof(q)} must be a positive value.");
            }

            currentQ = q;
            SetParameters(currentFilterType, currentF0, q);
        }

        /// <summary>
        /// Sets the current frequency and adjusts the <see cref="Filter"/> accordingly.
        /// </summary>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        public void SetFrequency(double frequency)
        {
            if (frequency <= 0)
            {
                throw new ArgumentException($"{nameof(frequency)} must be a positive non-zero value.");
            }

            currentF0 = frequency;
            SetParameters(currentFilterType, frequency, currentQ);
        }

        /// <summary>
        /// Sets the current frequency and adjusts the <see cref="Filter"/> accordingly.
        /// </summary>
        /// <param name="filterType">Any <see cref="FilterType"/>.</param>
        public void SetType(FilterType filterType)
        {
            currentFilterType = filterType;
            SetParameters(filterType, currentF0, currentQ);
        }

        /// <summary>
        /// Sets the samplerate of the <see cref="Filter"/>.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate.</param>
        public void SetSampleRate(int sampleRate)
        {
            if (sampleRate < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(sampleRate));
            }

            this.SampleRate = sampleRate;
            SetParameters(currentFilterType, currentF0, currentQ);
        }

        /// <summary>
        /// Sets the parameters and adjusts the <see cref="Filter"/> accordingly.
        /// </summary>
        /// <param name="filterType">Any <see cref="FilterType"/>.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="q">Any positive Q (quality factor).</param>
        public void SetAll(FilterType filterType, double frequency, double q)
        {
            if (frequency <= 0)
            {
                throw new ArgumentException($"{nameof(frequency)} must be a positive non-zero value.");
            }

            if (q < 0)
            {
                throw new ArgumentException($"{nameof(q)} must be a positive value.");
            }

            currentFilterType = filterType;
            currentF0 = frequency;
            currentQ = q;
            SetParameters(filterType, frequency, q);
        }

        private void SetParameters(FilterType filterType, double f0, double q)
        {
            double omega, sn, cs, alpha;
            double a0, a1, a2, b0, b1, b2;

            omega = 2f * Math.PI * f0 / SampleRate;
            sn = Math.Sin(omega);
            cs = Math.Cos(omega);
            alpha = sn / (2f * q);

            if (filterType == FilterType.LowPass)
            {
                // LPF
                b0 = (1f - cs) / 2f;
                b1 = 1f - cs;
                b2 = (1f - cs) / 2f;
                a0 = 1f + alpha;
                a1 = -2f * cs;
                a2 = 1f - alpha;
            }
            else
            {
                // HPF
                b0 = (1f + cs) / 2f;
                b1 = -(1f + cs);
                b2 = (1f + cs) / 2f;
                a0 = 1f + alpha;
                a1 = -2f * cs;
                a2 = 1f - alpha;
            }

            b0a0 = b0 / a0;
            b1a0 = b1 / a0;
            b2a0 = b2 / a0;
            a1a0 = a1 / a0;
            a2a0 = a2 / a0;
        }
    }
}