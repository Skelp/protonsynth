﻿using System;

namespace Skelp.ProtonSynth.SignalProcessors
{
    /// <summary>
    /// Filters out low frequencies and DC offsets gently.
    /// </summary>
    /// <remarks>Advisable to use as the last processor in the chain.</remarks>
    public class DCFilter : ISignalProcessor
    {
        // Alpha of this value is a good trade-off between speed and frequency range
        private const double Alpha = 0.9995;

        /// <summary>
        /// Initializes a new instance of the <see cref="DCFilter"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        public DCFilter(int sampleRate)
        {
            SetSampleRate(sampleRate);
        }

        /// <summary>
        /// Gets the samplerate of the <see cref="DCFilter"/>.
        /// </summary>
        public int SampleRate { get; private set; }

        private double RelativeAlpha { get; set; }

        private double PreviousW { get; set; } = 0;

        /// <summary>
        /// Sets the samplerate of the <see cref="DCFilter"/>.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate.</param>
        public void SetSampleRate(int sampleRate)
        {
            if (sampleRate < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(sampleRate));
            }

            SampleRate = sampleRate;

            // Alpha was choosen based on a samplerate of 44.1kHz
            RelativeAlpha = Alpha * 44100 / sampleRate;
        }

        /// <inheritdoc/>
        public double ProcessSample(double sample)
        {
            var oldW = PreviousW;
            PreviousW = sample + (RelativeAlpha * PreviousW);
            return PreviousW - oldW;
        }
    }
}