﻿using System;
using System.Collections.Generic;

namespace Skelp.ProtonSynth.SignalProcessors
{
    /// <summary>
    /// Represents a buffer with dynamic sizing. Can be resized any time.
    /// </summary>
    public class DynamicBuffer : List<double>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicBuffer"/> class.
        /// </summary>
        /// <param name="capacity">The initial size of the buffer.</param>
        public DynamicBuffer(int capacity)
            : base(capacity)
        {
            AddRange(new double[capacity]);
        }

        /// <summary>
        /// Resizes the <see cref="DynamicBuffer"/>.
        /// </summary>
        /// <param name="capacity">Any positive number.</param>
        public void Resize(int capacity)
        {
            // TODO This is too slow to use in practice. Try a fixed inital size with variable end-index instead.
            if (capacity < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity));
            }

            if (Count < capacity)
            {
                AddRange(new double[capacity - Count]);
            }
            else if (Count > capacity)
            {
                int itemsToRemove = Count - capacity;
                RemoveRange(Count - 1 - itemsToRemove, itemsToRemove);
            }
        }

        /// <summary>
        /// Calculates an index within the range of the <see cref="DynamicBuffer"/>.
        /// </summary>
        /// <param name="index">Any positive or negative value.</param>
        /// <returns>An index within the range of the <see cref="DynamicBuffer"/>.</returns>
        public int GetCircularIndex(int index) => index % (Count);
    }
}