﻿namespace Skelp.ProtonSynth.SignalProcessors
{
    /// <summary>
    /// An interface that describes any signal altering unit.
    /// </summary>
    public interface ISignalProcessor
    {
        /// <summary>
        /// Processes the given <paramref name="sample"/>.
        /// </summary>
        /// <param name="sample">Any value between -1 and 1.</param>
        /// <returns>The processed <paramref name="sample"/>.</returns>
        double ProcessSample(double sample);
    }
}