﻿using System;

namespace Skelp.ProtonSynth.SignalProcessors
{
    /// <summary>
    /// Provides a simple delay with feedback functionality.
    /// </summary>
    public class FeedbackDelay : ISignalProcessor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeedbackDelay"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        public FeedbackDelay(int sampleRate)
        {
            if (sampleRate <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sampleRate));
            }

            SampleRate = sampleRate;
            ResizeBuffer();
        }

        /// <summary>
        /// The amount of feedback from the previous delay to be added back into the signal.
        /// </summary>
        public UnitInterval Feedback { get; set; } = 0.5;

        /// <summary>
        /// The maximum, relative amplitude to the original signal the initial delay will have.
        /// </summary>
        public UnitInterval MaxAmplitude { get; set; } = 0.5;

        /// <summary>
        /// Gets the time it takes for an input signal to be returned by the <see cref="FeedbackDelay"/> instance.
        /// </summary>
        public TimeSpan DelayTime { get; private set; } = TimeSpan.FromSeconds(0.5);

        /// <summary>
        /// Gets the current samplerate the delay is running on.
        /// </summary>
        public int SampleRate { get; private set; }

        private DynamicBuffer Buffer { get; set; }

        private int CurrentIndex { get; set; }

        /// <summary>
        /// Resizes the delay internally to fit the new <paramref name="delayTime"/>.
        /// </summary>
        /// <param name="delayTime">Any valid <see cref="TimeSpan"/>.</param>
        public void Resize(TimeSpan delayTime)
        {
            DelayTime = delayTime;
            ResizeBuffer();

            // Index may be out of bounds due to resizing, recalculate
            CurrentIndex = Buffer.GetCircularIndex(CurrentIndex);
        }

        /// <summary>
        /// Updates the samplerate of the delay unit.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        public void SetSampleRate(int sampleRate)
        {
            if (sampleRate < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(sampleRate));
            }

            SampleRate = sampleRate;
            ResizeBuffer();
            CurrentIndex = Buffer.GetCircularIndex(CurrentIndex);
        }

        /// <inheritdoc/>
        public double ProcessSample(double sample)
        {
            CurrentIndex = Buffer.GetCircularIndex(CurrentIndex);
            Buffer[CurrentIndex] *= Feedback;
            Buffer[CurrentIndex] += (sample * MaxAmplitude);
            CurrentIndex++;
            return Buffer[Buffer.GetCircularIndex(CurrentIndex)];
        }

        private void ResizeBuffer()
        {
            Buffer = new DynamicBuffer((int)Math.Ceiling(SampleRate * DelayTime.TotalSeconds));
        }
    }
}