﻿using System;

namespace Skelp.ProtonSynth
{
    /// <summary>
    /// Ensures the endianess of data converted by <see cref="BitConverter"/>.
    /// </summary>
    internal static class EndianConverter
    {
        public static void SetBigEndian(byte[] data)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(data);
            }
        }

        public static void SetLittleEndian(byte[] data)
        {
            if (BitConverter.IsLittleEndian is false)
            {
                Array.Reverse(data);
            }
        }
    }
}