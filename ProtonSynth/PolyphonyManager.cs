﻿using System;
#if NET5_0_OR_GREATER || NETSTANDARD2_0
using System.Threading.Tasks;
#endif

namespace Skelp.ProtonSynth
{
    /// <summary>
    /// Represents simplified polyphony management of any number of <see cref="IVoice"/> instances.
    /// </summary>
    public class PolyphonyManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PolyphonyManager"/> class.
        /// </summary>
        /// <param name="voices">At least one <see cref="IVoice"/> instance.</param>
        public PolyphonyManager(params IVoice[] voices)
        {
            if (voices == null || voices.Length < 1)
            {
                throw new ArgumentException($"{nameof(voices)} most not be null nor have less than one {nameof(IVoice)} instance.");
            }

            Voices = voices;
            LatestVoiceSamples = new double[voices.Length];
        }

        /// <summary>
        /// Gets or sets the voices the <see cref="PolyphonyManager"/> manages.
        /// </summary>
        public IVoice[] Voices { get; set; }

        /// <summary>
        /// Gets or sets the maximum amplitude of samples from <see cref="GetSample"/> and <see cref="GetSamples(int)"/>.
        /// </summary>
        public UnitInterval Amplitude { get; set; } = 1;

        private double[] LatestVoiceSamples { get; set; }

        private int VoicesIndex { get; set; }

        private bool IsPanicing { get; set; }

        /// <summary>
        /// Stops all processing for all voices.
        /// </summary>
        public void Panic()
        {
            IsPanicing = true;
        }

        /// <summary>
        /// Resets Panic mode as well as anything the <see cref="Voices"/> offer to be reset.
        /// </summary>
        public void Reset()
        {
            // TODO Implement structural IVoice resets (reset stored Filter state, Oscillator.Cycle, etc.)
            IsPanicing = false;
        }

        /// <summary>
        /// Processes all <see cref="Voices"/> by calling their respective <see cref="IVoice.GetSample"/>.
        /// </summary>
        /// <returns>The summed sample of all <see cref="Voices"/> multiplied by <see cref="Amplitude"/>.</returns>
        public double GetSample()
        {
            if (IsPanicing)
            {
                return 0;
            }

            for (int i = 0; i < Voices.Length; i++)
            {
                LatestVoiceSamples[i] = Voices[i].GetSample();
            }

            double finalResult = 0;

            for (int i = 0; i < LatestVoiceSamples.Length; i++)
            {
                finalResult += LatestVoiceSamples[i];
            }

            return finalResult * Amplitude;
        }

        /// <summary>
        /// Processes all <see cref="Voices"/> in parallel by calling their respective <see cref="IVoice.GetSample"/>.
        /// </summary>
        /// <returns>The summed samples of all <see cref="Voices"/> multiplied by <see cref="Amplitude"/>.</returns>
        /// <remarks>.NET Standard 1.0 does not support the Parallel library and will run this method sequentially instead.</remarks>
        public double[] GetSamples(int amount)
        {
            if (IsPanicing)
            {
                return new double[0];
            }

            var tempVoiceResults = new double[Voices.Length][];

#if NETSTANDARD1_0
            // .NET Standard 1.0 does not support the Parallel library
            for (int i = 0; i < LatestVoiceSamples.Length; i++)
            {
                tempVoiceResults[i] = Voices[i].GetSamples(amount);
            }
#else
            Parallel.For(0, Voices.Length, (i) =>
            {
                tempVoiceResults[i] = Voices[i].GetSamples(amount);
            });
#endif

            double[] finalResults = new double[amount];

            for (int i = 0; i < tempVoiceResults.Length; i++)
            {
                for (int c = 0; c < amount; c++)
                {
                    finalResults[c] += tempVoiceResults[i][c];
                }
            }

            return finalResults;
        }

        /// <summary>
        /// Takes in any note off signal and forwards it to the oldest <see cref="IVoice"/> in <see cref="Voices"/>.
        /// </summary>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitude">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        public void ProcessNoteOn(double frequency, UnitInterval amplitude)
        {
            if (frequency <= 0)
            {
                throw new ArgumentException($"{nameof(frequency)} must be a positive non-zero value.");
            }

            if (amplitude == 0)
            {
                // Note on commands with a velocity / amplitude of 0 are considered a note off command
                ProcessNoteOff(frequency);
                return;
            }

            if (VoicesIndex > Voices.Length - 1)
            {
                VoicesIndex = 0;
            }

            Voices[VoicesIndex].ProcessNoteOn(frequency, amplitude);
            VoicesIndex++;
        }

        /// <summary>
        /// Takes in any note off signal and finds the oldest <see cref="IVoice"/> which is still playing at a <paramref name="frequency"/>.
        /// </summary>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        public void ProcessNoteOff(double frequency)
        {
            if (frequency <= 0)
            {
                throw new ArgumentException($"{nameof(frequency)} must be a positive non-zero value.");
            }

            for (int i = 0; i < Voices.Length; i++)
            {
                if ((Voices[i].CurrentFrequency == frequency) == false)
                {
                    continue;
                }

                if (Voices[i].IsPlaying == false)
                {
                    // Cannot turn of a note that is not playing, must find next voice with the same frequency
                    continue;
                }

                Voices[i].ProcessNoteOff();
                break;
            }
        }

        /// <summary>
        /// Processes pitchbend signals and forwards them to all <see cref="IVoice"/> instances.
        /// </summary>
        /// <param name="bendPosition">Any valid <see cref="UnitInterval"/>. Value 0.5 represents the middle of the <see cref="PitchBend"/>.</param>
        public void ProcessPitchBend(UnitInterval bendPosition)
        {
            for (int i = 0; i < Voices.Length; i++)
            {
                Voices[i].ProcessPitchBend(bendPosition);
            }
        }

        // TODO Add further processing methods
    }
}