﻿namespace Skelp.ProtonSynth
{
    /// <summary>
    /// Implementation of "Fast and accurate sine/cosine approximation" (Michael Baczynski) by Martin Climatiano.
    /// <para>
    /// http://www.mclimatiano.com/faster-sine-approximation-using-quadratic-curve/.
    /// </para>
    /// </summary>
    internal static class SineHP
    {
        private static float sin = 0;

        public static float Sin(float x)
        {
            if (x < -3.14159265f)
            {
                x += 6.28318531f;
            }
            else
                if (x > 3.14159265f)
            {
                x -= 6.28318531f;
            }

            if (x < 0)
            {
                sin = x * (1.27323954f + (0.405284735f * x));

                if (sin < 0)
                {
                    sin = sin * ((-0.255f * (sin + 1)) + 1);
                }
                else
                {
                    sin = sin * ((0.255f * (sin - 1)) + 1);
                }
            }
            else
            {
                sin = x * (1.27323954f - (0.405284735f * x));

                if (sin < 0)
                {
                    sin = sin * ((-0.255f * (sin + 1)) + 1);
                }
                else
                {
                    sin = sin * ((0.255f * (sin - 1)) + 1);
                }
            }

            return sin;
        }
    }
}