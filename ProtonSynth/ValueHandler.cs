﻿namespace Skelp.ProtonSynth
{
    public static class ValueHandler
    {
        public static double ChangeRange(double value, double fromMin, double fromMax, double toMin, double toMax)
        {
            return ((value - fromMin) * (toMax - toMin) / (fromMax - fromMin)) + toMin;
        }

        /// <summary>
        /// Calculates an interpolated value of <paramref name="x1"/> and <paramref name="x2"/> with the <paramref name="ratio"/> provided.
        /// </summary>
        /// <param name="x1">Any value.</param>
        /// <param name="x2">Any value.</param>
        /// <param name="ratio">Any number between 0 and 1 where 0 is 100% x1 and 1 is 100% x2.</param>
        /// <returns>The interpolated result of the operation.</returns>
        public static double Interpolate(double x1, double x2, double ratio)
        {
            return (x1 * (1 - ratio)) + (x2 * ratio);
        }
    }
}