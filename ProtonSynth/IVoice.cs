﻿namespace Skelp.ProtonSynth
{
    /// <summary>
    /// Represents any constructed synthesizer voice to be used in a polymorphic synthesizer.
    /// </summary>
    /// <remarks>Suggested application for this interface is in conjunction with <see cref="PolyphonyManager"/>.</remarks>
    public interface IVoice
    {
        /// <summary>
        /// Gets the frequency that was last sent to the <see cref="IVoice"/>.
        /// </summary>
        /// <remarks>Does not reflect the actual frequency, only the original input-frequency when calling <see cref="ProcessNoteOn(double, UnitInterval)"/>.</remarks>
        double CurrentFrequency { get; }

        /// <summary>
        /// Gets a value indicating whether <see cref="ProcessNoteOn(double, UnitInterval)"/> or <see cref="ProcessNoteOff"/> was last called.
        /// </summary>
        bool IsPlaying { get; }

        /// <summary>
        /// Processes the entire pipeline of the <see cref="IVoice"/>.
        /// </summary>
        /// <returns>The next sample according to the inner state of the <see cref="IVoice"/>.</returns>
        double GetSample();

        /// <summary>
        /// Processes the entire pipeline of the <see cref="IVoice"/>.
        /// </summary>
        /// <returns>The next n-samples according to the inner state of the <see cref="IVoice"/>.</returns>
        double[] GetSamples(int amount);

        /// <summary>
        /// Aligns the inner state of the <see cref="IVoice"/> and processes incoming note on signals.
        /// </summary>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitude">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        void ProcessNoteOn(double frequency, UnitInterval amplitude);

        /// <summary>
        /// Processes the note off signal by setting the state of relevant members in <see cref="IVoice"/>.
        /// </summary>
        void ProcessNoteOff();

        /// <summary>
        /// Processes incoming pitchbend signals.
        /// </summary>
        /// <param name="bendPosition">Any valid <see cref="UnitInterval"/>. Value 0.5 represents the middle of the <see cref="PitchBend"/>.</param>
        void ProcessPitchBend(UnitInterval bendPosition);

        // TODO Add further processing methods
    }
}