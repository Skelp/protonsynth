﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Skelp.ProtonSynth
{
    // Thanks to this helpful website for providing information on the RIFF WAV standard: http://soundfile.sapp.org/doc/WaveFormat/

    /// <summary>
    /// Provides methods to create a functional RIFF WAVE data structure.
    /// </summary>
    public class WaveBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WaveBuilder"/> class.
        /// </summary>
        public WaveBuilder()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveBuilder"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive number more than 0.</param>
        /// <param name="sampleConverter">Any valid, non-null <see cref="ProtonSynth.SampleConverter"/> instance.</param>
        public WaveBuilder(int sampleRate, SampleConverter sampleConverter)
        {
            SetSampleRate(sampleRate);
            SetSampleConverter(sampleConverter);
        }

        private int SampleRate { get; set; }

        private SampleConverter SampleConverter { get; set; }

        private List<double> Samples { get; set; } = new List<double>();

        /// <summary>
        /// Sets the samplerate of the <see cref="WaveBuilder"/> instance.
        /// </summary>
        /// <param name="value">Any positive number more than 0.</param>
        public void SetSampleRate(int value)
        {
            if (value is < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "value must be positive and more than 0.");
            }

            SampleRate = value;
        }

        /// <summary>
        /// Sets the <see cref="SampleConverter"/> of the <see cref="WaveBuilder"/> instance.
        /// </summary>
        /// <param name="value">Any valid, non-null <see cref="ProtonSynth.SampleConverter"/> instance.</param>
        public void SetSampleConverter(SampleConverter value)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            SampleConverter = value;
        }

        /// <summary>
        /// Adds a single sample to the <see cref="WaveBuilder"/> instance.
        /// </summary>
        /// <param name="sample">Any value between -1 and +1.</param>
        public void AddSample(double sample)
        {
            if (sample is < -1 or > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(sample), "sample must be between -1 and +1.");
            }

            Samples.Add(sample);
        }

        /// <summary>
        /// Adds samples to the <see cref="WaveBuilder"/> instance.
        /// </summary>
        /// <param name="samples">Any collection with values between -1 and +1.</param>
        public void AddSamples(IEnumerable<double> samples)
        {
            foreach (var sample in samples)
            {
                AddSample(sample);
            }
        }

        /// <summary>
        /// Constructs the final RIFF WAVE file data.
        /// </summary>
        /// <returns>A <see cref="byte"/> array containing the assembled RIFF WAVE file data.</returns>
        public byte[] ToByteArray()
        {
            var result = new List<byte>();
            AppendHeader(result);

            foreach (var sample in Samples)
            {
                AppendSample(sample, result);
            }

            return result.ToArray();
        }

        private void AppendHeader(List<byte> data)
        {
            // ChunkID
            var riff = Encoding.UTF8.GetBytes("RIFF");
            data.AddRange(riff);

            // ChunkSize
            var chunkSize = BitConverter.GetBytes((uint)(36 + Samples.Count * SampleConverter.BitDepth / 8));
            EndianConverter.SetLittleEndian(chunkSize);
            data.AddRange(chunkSize);

            // Format
            var wave = Encoding.UTF8.GetBytes("WAVE");
            data.AddRange(wave);

            // Subchunk1ID
            var fmt = Encoding.UTF8.GetBytes("fmt ");
            data.AddRange(fmt);

            // Subchunk1Size
            var subchunk1Size = BitConverter.GetBytes((uint)16);
            EndianConverter.SetLittleEndian(subchunk1Size);
            data.AddRange(subchunk1Size);

            // AudioFormat
            var audioFormat = BitConverter.GetBytes((ushort)1);
            EndianConverter.SetLittleEndian(audioFormat);
            data.AddRange(audioFormat);

            // NumChannels
            var numChannels = BitConverter.GetBytes((ushort)1);
            EndianConverter.SetLittleEndian(numChannels);
            data.AddRange(numChannels);

            // SampleRate
            var sampleRate = BitConverter.GetBytes((uint)SampleRate);
            EndianConverter.SetLittleEndian(sampleRate);
            data.AddRange(sampleRate);

            // ByteRate
            var byteRate = BitConverter.GetBytes((uint)(SampleRate * SampleConverter.BitDepth / 8));
            EndianConverter.SetLittleEndian(byteRate);
            data.AddRange(byteRate);

            // BlockAlign
            var blockAlign = BitConverter.GetBytes((ushort)(SampleConverter.BitDepth / 8));
            EndianConverter.SetLittleEndian(blockAlign);
            data.AddRange(blockAlign);

            // BitsPerSample
            var bitsPerSample = BitConverter.GetBytes((ushort)SampleConverter.BitDepth);
            EndianConverter.SetLittleEndian(bitsPerSample);
            data.AddRange(bitsPerSample);

            // Subchunk2ID
            var dataString = Encoding.UTF8.GetBytes("data");
            data.AddRange(dataString);

            // Subchunk2Size
            var subchunk2Size = BitConverter.GetBytes((uint)(Samples.Count * SampleConverter.BitDepth / 8));
            EndianConverter.SetLittleEndian(subchunk2Size);
            data.AddRange(subchunk2Size);
        }

        private void AppendSample(double sample, List<byte> data)
        {
            byte[] result = SampleConverter.ConvertSample(sample);
            EndianConverter.SetLittleEndian(result);
            data.AddRange(result);
        }
    }
}