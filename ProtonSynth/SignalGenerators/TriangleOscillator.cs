﻿using System;

namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Implementation of a triangle wave <see cref="Oscillator"/>.
    /// </summary>
    public class TriangleOscillator : Oscillator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TriangleOscillator"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitudeMultiplier">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        public TriangleOscillator(int sampleRate, double frequency = 440, double amplitudeMultiplier = 1)
            : base(sampleRate, frequency, amplitudeMultiplier)
        {
        }

        /// <inheritdoc/>
        public override double GetSample()
        {
            // Using the fast, inaccurate sine as there seems to be no
            // impact on the quality of the resulting triangle waveform
            var result = 2 * AmplitudeMultiplier / Math.PI * Math.Asin(SineHP.Sin((float)GetAngularVelocity((Cycle + Phase) % 1))); ;
            CalculateCycle();
            return result;
        }
    }
}