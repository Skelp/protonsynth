﻿namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Implementation of a pseudo random noise <see cref="ISignalGenerator"/>.
    /// </summary>
    public class NoiseGenerator : ISignalGenerator
    {
        /// <inheritdoc/>
        public UnitInterval AmplitudeMultiplier { get; set; } = 1;

        private XorShiftRandom Random { get; set; } = new XorShiftRandom();

        /// <inheritdoc/>
        public double GetSample()
        {
            return ((Random.NextDouble() * 2) - 1) * AmplitudeMultiplier;
        }
    }
}