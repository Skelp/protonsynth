﻿namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Implementation of an approximation of a sine <see cref="Oscillator"/>.
    /// <para>
    /// Reduces computation time but introduces harmonics.
    /// </para>
    /// </summary>
    public class FastSineOscillator : Oscillator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FastSineOscillator"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitudeMultiplier">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        public FastSineOscillator(int sampleRate, double frequency = 440, double amplitudeMultiplier = 1)
            : base(sampleRate, frequency, amplitudeMultiplier)
        {
        }

        /// <inheritdoc/>
        public override double GetSample()
        {
            var result = SineHP.Sin((float)GetAngularVelocity((Cycle + Phase) % 1)) * AmplitudeMultiplier;
            CalculateCycle();
            return result;
        }
    }
}