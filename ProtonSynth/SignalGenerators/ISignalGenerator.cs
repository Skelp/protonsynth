﻿namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// An interface that describes any signal source.
    /// </summary>
    public interface ISignalGenerator
    {
        /// <summary>
        /// Gets or sets the maximum amplitude of the signal.
        /// </summary>
        UnitInterval AmplitudeMultiplier { get; set; }

        /// <summary>
        /// Gets the next sample according to the inner state of the <see cref="ISignalGenerator"/>.
        /// </summary>
        /// <returns>A <see cref="double"/> with a maximum range between -1 to 1.</returns>
        double GetSample();
    }
}