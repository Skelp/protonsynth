﻿using System;

namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Barebone implementation of <see cref="IOscillator"/>.
    /// </summary>
    public abstract class Oscillator : IOscillator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Oscillator"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitudeMultiplier">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        private protected Oscillator(int sampleRate = 441000, double frequency = 440, double amplitudeMultiplier = 1)
        {
            if (sampleRate <= 0)
            {
                throw new ArgumentException($"{nameof(sampleRate)} must be a positive non-zero value.");
            }

            if (frequency <= 0)
            {
                throw new ArgumentException($"{nameof(frequency)} must be a positive non-zero value.");
            }

            SampleRate = sampleRate;
            Frequency = frequency;
            AmplitudeMultiplier = amplitudeMultiplier;
        }

        /// <inheritdoc/>
        public double Frequency { get; set; }

        /// <inheritdoc/>
        public int SampleRate { get; private protected set; }

        /// <inheritdoc/>
        public UnitInterval AmplitudeMultiplier { get; set; } = 1;

        /// <inheritdoc/>
        public UnitInterval Phase { get; set; }

        /// <summary>
        /// Gets or sets the current position of the <see cref="Oscillator"/> relative to <see cref="Frequency"/>.
        /// </summary>
        private protected double Cycle { get; set; }

        /// <inheritdoc/>
        public abstract double GetSample();

        /// <inheritdoc/>
        public void ResetCycle()
        {
            Cycle = 0;
        }

        /// <inheritdoc/>
        public void SetSampleRate(int sampleRate)
        {
            if (sampleRate < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(sampleRate));
            }

            SampleRate = sampleRate;
        }

        /// <summary>
        /// Advances <see cref="Cycle"/> for the next sample in accordance with <see cref="Frequency"/> and <see cref="SampleRate"/>.
        /// </summary>
        private protected void CalculateCycle()
        {
            Cycle = (Cycle + (Frequency / SampleRate)) % 1;
        }

        /// <summary>
        /// Calculates the angular velocity for a given <paramref name="frequency"/>.
        /// </summary>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <returns>The angular velocity of <paramref name="frequency"/>.</returns>
        private protected double GetAngularVelocity(double frequency)
        {
            return frequency * 2 * Math.PI;
        }
    }
}