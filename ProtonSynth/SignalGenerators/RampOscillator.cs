﻿namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Implementation of a ramp <see cref="Oscillator"/>.
    /// </summary>
    public class RampOscillator : SawtoothOscillator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RampOscillator"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitudeMultiplier">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        public RampOscillator(int sampleRate, double frequency = 440, double amplitudeMultiplier = 1)
            : base(sampleRate, frequency, amplitudeMultiplier)
        {
        }

        /// <inheritdoc/>
        public override double GetSample()
        {
            return base.GetSample() * -1;
        }
    }
}