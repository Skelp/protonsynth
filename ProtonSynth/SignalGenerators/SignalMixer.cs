﻿using System;
using System.Collections.Generic;

namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Helper class to mix signal sources together whilst staying within the sample bounds of -1 and +1.
    /// </summary>
    public class SignalMixer : ISignalGenerator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SignalMixer"/> class.
        /// </summary>
        /// <param name="signalGenerators">Any collection of <see cref="ISignalGenerator"/> instances that is not NULL and has valid items.</param>
        public SignalMixer(IEnumerable<ISignalGenerator> signalGenerators)
        {
            if (signalGenerators is null)
            {
                throw new ArgumentNullException(nameof(signalGenerators));
            }

            SignalGenerators = signalGenerators;

            foreach (var generator in signalGenerators)
            {
                if (generator is null)
                {
                    throw new ArgumentNullException(nameof(generator));
                }

                GeneratorCount++;
            }

            if (GeneratorCount == 0)
            {
                throw new ArgumentException(nameof(signalGenerators));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SignalMixer"/> class.
        /// </summary>
        /// <param name="signalGenerators">Any amount of <see cref="ISignalGenerator"/> instances that is not NULL and has valid items.</param>
        public SignalMixer(params ISignalGenerator[] signalGenerators)
        {
            if (signalGenerators is null)
            {
                throw new ArgumentNullException(nameof(signalGenerators));
            }

            if (signalGenerators.Length == 0)
            {
                throw new ArgumentException(nameof(signalGenerators));
            }

            SignalGenerators = signalGenerators;

            foreach (var generator in signalGenerators)
            {
                if (generator is null)
                {
                    throw new ArgumentNullException(nameof(generator));
                }
            }

            GeneratorCount = signalGenerators.Length;
        }

        /// <summary>
        /// Gets the collection of <see cref="ISignalGenerator"/> instances of the <see cref="SignalMixer"/>.
        /// </summary>
        public IEnumerable<ISignalGenerator> SignalGenerators { get; }

        /// <inheritdoc/>
        public UnitInterval AmplitudeMultiplier { get; set; } = 1;

        private int GeneratorCount { get; set; }

        /// <inheritdoc/>
        public double GetSample()
        {
            double result = 0;

            foreach (var generator in SignalGenerators)
            {
                result += generator.GetSample();
            }

            return result / GeneratorCount * AmplitudeMultiplier;
        }
    }
}