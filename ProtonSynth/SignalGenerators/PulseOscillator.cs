﻿namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Implementation of a pulse <see cref="Oscillator"/>.
    /// </summary>
    public class PulseOscillator : Oscillator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PulseOscillator"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitudeMultiplier">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        /// <param name="pulseWidth">Any positive pulse width between 0 and 1.</param>
        public PulseOscillator(int sampleRate, double frequency = 440, double amplitudeMultiplier = 1, double pulseWidth = 0.5)
            : base(sampleRate, frequency, amplitudeMultiplier)
        {
            PulseWidth = pulseWidth;
        }

        /// <summary>
        /// Gets or sets the pulse width or duty cycle of the <see cref="PulseOscillator"/>.
        /// </summary>
        public UnitInterval PulseWidth { get; set; }

        /// <inheritdoc/>
        public override double GetSample()
        {
            var result = ((1 - ((Cycle + Phase) % 1)) > PulseWidth ? 1 : -1) * AmplitudeMultiplier;
            CalculateCycle();
            return result;
        }
    }
}