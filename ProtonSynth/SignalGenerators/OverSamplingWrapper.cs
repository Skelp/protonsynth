﻿using System;

namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Wrapper class to be used with <see cref="IOscillator"/> instances to automate oversampling and samplerate updates.
    /// </summary>
    public class OverSamplingWrapper : ISignalGenerator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OverSamplingWrapper"/> class.
        /// </summary>
        /// <param name="oscillator">Any oscillator.</param>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="overSamplingFactor">Any positive factor.</param>
        public OverSamplingWrapper(IOscillator oscillator, int sampleRate, int overSamplingFactor)
        {
            if (oscillator == null)
            {
                throw new ArgumentNullException(nameof(oscillator));
            }

            if (overSamplingFactor <= 0)
            {
                throw new ArgumentException($"{nameof(overSamplingFactor)} must be a positive non-zero value.");
            }

            OutputSampleRate = sampleRate;
            OverSampledSampleRate = sampleRate * overSamplingFactor;
            OverSamplingFactor = overSamplingFactor;
            Oscillator = oscillator;
            oscillator.SetSampleRate(OverSampledSampleRate);
        }

        /// <summary>
        /// Gets the final samplerate the <see cref="OverSamplingWrapper"/> produces in <see cref="GetSample"/>.
        /// </summary>
        public int OutputSampleRate { get; private set; }

        /// <summary>
        /// Gets the internal samplerate the <see cref="OverSamplingWrapper"/> produces in <see cref="GetSample"/> before being downsampled.
        /// </summary>
        public int OverSampledSampleRate { get; private set; }

        /// <summary>
        /// Gets the internal oversampling factor.
        /// </summary>
        public int OverSamplingFactor { get; private set; }

        /// <summary>
        /// Gets the <see cref="Oscillator"/> which is being oversampled in <see cref="GetSample"/>.
        /// </summary>
        public IOscillator Oscillator { get; private set; }

        /// <inheritdoc/>
        public UnitInterval AmplitudeMultiplier { get; set; } = 1;

        /// <summary>
        /// Takes oversampling into consideration when asking the <see cref="Oscillator"/> to generate a sample.
        /// </summary>
        /// <returns>An oversampled sample of <see cref="Oscillator"/> in accordance to the wrapper state.</returns>
        public double GetSample()
        {
            double sample = 0;

            for (int i = 0; i < OverSamplingFactor; i++)
            {
                sample += Oscillator.GetSample();
            }

            return sample / OverSamplingFactor * AmplitudeMultiplier;
        }

        /// <summary>
        /// Updates the <see cref="OverSamplingWrapper"/> according to the new <paramref name="sampleRate"/>.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        public void SetSampleRate(int sampleRate)
        {
            OutputSampleRate = sampleRate;
            OverSampledSampleRate = sampleRate * OverSamplingFactor;
            Oscillator.SetSampleRate(OverSampledSampleRate);
        }

        /// <summary>
        /// Updates the <see cref="OverSamplingWrapper"/> according to the new <paramref name="overSamplingFactor"/>.
        /// </summary>
        /// <param name="overSamplingFactor">Any positive factor.</param>
        public void SetOverSamplingFactor(int overSamplingFactor)
        {
            if (overSamplingFactor <= 0)
            {
                throw new ArgumentException($"{nameof(overSamplingFactor)} must be a positive non-zero value.");
            }

            OverSamplingFactor = overSamplingFactor;
            OverSampledSampleRate = OutputSampleRate * OverSamplingFactor;
            Oscillator.SetSampleRate(OverSampledSampleRate);
        }
    }
}