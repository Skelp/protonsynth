﻿namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// An interface that describes any oscillating signal source.
    /// </summary>
    public interface IOscillator : ISignalGenerator
    {
        /// <summary>
        /// Gets or sets the frequency of the oscillator.
        /// </summary>
        double Frequency { get; set; }

        /// <summary>
        /// Gets or sets the phase of the oscillator where a value of 0 represents 0 degrees and a value of 1 represents 360 degrees.
        /// </summary>
        UnitInterval Phase { get; set; }

        /// <summary>
        /// Gets the samplerate of the <see cref="IOscillator"/>.
        /// </summary>
        int SampleRate { get; }

        /// <summary>
        /// Resets the internal cycle of the <see cref="IOscillator"/>.
        /// </summary>
        void ResetCycle();

        /// <summary>
        /// Sets the samplerate of the <see cref="IOscillator"/>.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate.</param>
        void SetSampleRate(int sampleRate);
    }
}