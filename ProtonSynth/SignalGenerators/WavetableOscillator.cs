﻿using System;

namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Implementation of a wavetable <see cref="Oscillator"/>.
    /// </summary>
    public class WavetableOscillator : Oscillator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WavetableOscillator"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitudeMultiplier">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        public WavetableOscillator(int sampleRate, double frequency = 440, double amplitudeMultiplier = 1)
            : base(sampleRate, frequency, amplitudeMultiplier)
        {
        }

        /// <summary>
        /// Gets or sets the current waveform table of the <see cref="WavetableOscillator"/> of any given size.
        /// </summary>
        public Waveform[] WaveformTable { get; set; }

        /// <summary>
        /// Gets or sets the position within the <see cref="WaveformTable"/> where 0 represents the beginning and 1 the end of the array.
        /// </summary>
        public UnitInterval TablePosition { get; set; }

        /// <inheritdoc/>
        public override double GetSample()
        {
            var tableIndex = ValueHandler.ChangeRange(TablePosition, 0, 1, 0, WaveformTable.Length - 1);

            // Ratio between two neighboring waveforms
            var waveformRatio = tableIndex % 1;

            // The actual indecies of these two waveforms
            var firstWaveformIndex = (int)Math.Truncate(tableIndex);
            var secondWaveformIndex = (firstWaveformIndex + 1) % (WaveformTable.Length);

            var finalCycle = (Cycle + Phase) % 1;

            // Untruncated indecies of the samples according to this.Cycle
            var firstWaveformSampleIndex = (WaveformTable[firstWaveformIndex].Samples.Length - 1) * finalCycle;
            var secondWaveformSampleIndex = (WaveformTable[secondWaveformIndex].Samples.Length - 1) * finalCycle;

            var truncatedFirstIndex = (int)Math.Truncate(firstWaveformSampleIndex);
            var truncatedSecondIndex = (int)Math.Truncate(secondWaveformSampleIndex);

            var interpolated1stSample = ValueHandler.Interpolate(
                WaveformTable[firstWaveformIndex].Samples[truncatedFirstIndex],
                WaveformTable[firstWaveformIndex].Samples[truncatedFirstIndex + 1],
                truncatedFirstIndex % 1);

            var interpolated2ndSample = ValueHandler.Interpolate(
                WaveformTable[secondWaveformIndex].Samples[truncatedSecondIndex],
                WaveformTable[secondWaveformIndex].Samples[truncatedSecondIndex + 1],
                truncatedSecondIndex % 1);

            var result = ValueHandler.Interpolate(interpolated1stSample, interpolated2ndSample, waveformRatio) * AmplitudeMultiplier;
            CalculateCycle();
            return result;
        }
    }
}