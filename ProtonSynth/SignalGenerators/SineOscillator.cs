﻿using System;

namespace Skelp.ProtonSynth.SignalGenerators
{
    /// <summary>
    /// Implementation of a sine <see cref="Oscillator"/>.
    /// </summary>
    public class SineOscillator : Oscillator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SineOscillator"/> class.
        /// </summary>
        /// <param name="sampleRate">Any positive samplerate more than zero.</param>
        /// <param name="frequency">Any positive frequency more than zero.</param>
        /// <param name="amplitudeMultiplier">Any value between <see cref="UnitInterval.MinValue"/> and <see cref="UnitInterval.MaxValue"/>.</param>
        public SineOscillator(int sampleRate, double frequency = 440, double amplitudeMultiplier = 1)
            : base(sampleRate, frequency, amplitudeMultiplier)
        {
        }

        /// <inheritdoc/>
        public override double GetSample()
        {
            var result = Math.Sin(GetAngularVelocity((Cycle + Phase) % 1)) * AmplitudeMultiplier;
            CalculateCycle();
            return result;
        }
    }
}