﻿using System;

namespace Skelp.ProtonSynth
{
    /// <summary>
    /// Provides an array of frequencies for every midi note from 0 to 127.
    /// </summary>
    public class MidiFrequencyArray
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MidiFrequencyArray"/> class.
        /// </summary>
        /// <param name="baseFrequency">Any positive frequency in Hz which represents the frequency of A4.</param>
        public MidiFrequencyArray(double baseFrequency = 440.0)
        {
            if (baseFrequency <= 0)
            {
                throw new ArgumentException($"{nameof(baseFrequency)} must be a positive non-zero value.");
            }

            BaseFrequency = baseFrequency;
            Frequencies = new double[128];

            for (byte i = 0; i < 128; i++)
            {
                Frequencies[i] = MidiNoteToFrequency(i);
            }
        }

        /// <summary>
        /// Gets the frequency the array was originally constructed with.
        /// </summary>
        public double BaseFrequency { get; }

        private double[] Frequencies { get; }

        /// <summary>
        /// Gets the corresponding frequency in Hz.
        /// </summary>
        /// <param name="midiNote">Any valid midi note from 0 to 127.</param>
        /// <returns>A <see cref="double"/> representing the frequency in Hz.</returns>
        public double this[int midiNote]
        {
            get
            {
                return Frequencies[midiNote];
            }
        }

        internal static double MidiNoteToFrequency(byte midiNote)
        {
            return 440.0 * Math.Pow(2.0, (midiNote - 69.0) / 12.0);
        }
    }
}