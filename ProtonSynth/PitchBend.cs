﻿using System;

namespace Skelp.ProtonSynth
{
    /// <summary>
    /// Implementation of a pitchbend.
    /// </summary>
    public class PitchBend
    {
        private double frequency;

        private int semitoneRange;

        /// <summary>
        /// Initializes a new instance of the <see cref="PitchBend"/> class.
        /// </summary>
        /// <param name="frequency">The frequency the pitchbending should be based around.</param>
        /// <param name="semitoneRange">The range in which the pitch is bent <paramref name="semitoneRange"/> semitones in one direction.</param>
        public PitchBend(double frequency, int semitoneRange)
        {
            Frequency = frequency;
            SemitoneRange = semitoneRange;
        }

        /// <summary>
        /// Gets or sets the base frequency.
        /// </summary>
        public double Frequency
        {
            get
            {
                return frequency;
            }

            set
            {
                frequency = value > 0 ? value : throw new ArgumentException($"{nameof(value)} must be a positive non-zero value.");
            }
        }

        /// <summary>
        /// Gets or sets the value representing the difference in semitones from <see cref="Frequency"/> in one direction.
        /// </summary>
        /// <remarks>The total semitone range of the <see cref="PitchBend"/> module is 2 * <see cref="SemitoneRange"/> + 1.</remarks>
        public int SemitoneRange
        {
            get
            {
                return semitoneRange;
            }

            set
            {
                semitoneRange = value > 0 ? value : throw new ArgumentException($"{nameof(value)} must be a positive non-zero value.");
            }
        }

        /// <summary>
        /// Gets or sets the relative position of the <see cref="PitchBend"/>. 0.5 represents a neutral position.
        /// </summary>
        public UnitInterval BendPosition { get; set; } = 0.5;

        /// <summary>
        /// Calculates the frequency difference to <see cref="Frequency"/> based on the <see cref="SemitoneRange"/> and the current <see cref="BendPosition"/>.
        /// </summary>
        /// <returns>The frequency difference in Hz.</returns>
        public double GetFrequencyDelta()
        {
            return Frequency * (GetIntervalRatio(ValueHandler.ChangeRange(BendPosition, 0, 1, SemitoneRange * -1, SemitoneRange)) - 1);
        }

        private double GetIntervalRatio(double semitones) => Math.Pow(2, semitones / 12d);
    }
}