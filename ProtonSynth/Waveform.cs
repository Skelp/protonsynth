﻿namespace Skelp.ProtonSynth
{
    /// <summary>
    /// Container for a single cycle of a waveform.
    /// </summary>
    public class Waveform
    {
        /// <summary>
        /// Gets or sets the waveform data of any given length.
        /// </summary>
        public double[] Samples { get; set; }

        /// <summary>
        /// Gets or sets the position within the cycle to start looping from.
        /// </summary>
        public UnitInterval StartPosition { get; set; } = 0;

        /// <summary>
        /// Gets or sets the last position within the cycle before going back to <see cref="StartPosition"/>.
        /// </summary>
        public UnitInterval EndPosition { get; set; } = 1;
    }
}