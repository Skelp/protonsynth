﻿/*===============================[ XorShiftPlus ]==============================
  ==-------------[ (c) 2018 R. Wildenhaus - Licensed under MIT ]-------------==
  ============================================================================= */

using System;

namespace Skelp.ProtonSynth
{
    /// <summary>
    ///   Generates pseudorandom primitive types with a 64-bit implementation
    ///   of the XorShift algorithm.
    /// </summary>
    internal class XorShiftRandom
    {
        // Constants
        private const double DoubleUnit = 1.0 / (int.MaxValue + 1.0);

        // State Fields
        private ulong x;
        private ulong y;

        /// <summary>
        ///   Constructs a new  generator using two
        ///   random Guid hash codes as a seed.
        /// </summary>
        public XorShiftRandom()
        {
            x = (ulong)Guid.NewGuid().GetHashCode();
            y = (ulong)Guid.NewGuid().GetHashCode();
        }

        /// <summary>
        ///   Constructs a new  generator
        ///   with the supplied seed.
        /// </summary>
        /// <param name="seed">
        ///   The seed value.
        /// </param>
        public XorShiftRandom(ulong seed)
        {
            x = seed << 3;
            x = seed >> 3;
        }

        /// <summary>
        ///   Generates a pseudorandom double between
        ///   0 and 1 non-inclusive.
        /// </summary>
        /// <returns>
        ///   A pseudorandom double.
        /// </returns>
        public double NextDouble()
        {
            double result;
            ulong tempX, tempY, tempZ;

            tempX = y;
            x ^= x << 23;
            tempY = x ^ y ^ (x >> 17) ^ (y >> 26);

            tempZ = tempY + y;
            result = DoubleUnit * (0x7FFFFFFF & tempZ);

            x = tempX;
            y = tempY;

            return result;
        }
    }
}