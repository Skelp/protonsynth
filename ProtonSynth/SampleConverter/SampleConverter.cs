﻿namespace Skelp.ProtonSynth
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// Implementation similar to Microsofts <see cref="System.Text.Encoding"/> class.
    /// </remarks>
    public abstract class SampleConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SampleConverter"/> class.
        /// </summary>
        protected SampleConverter() { }

        /// <summary>
        /// Gets a new <see cref="SampleConverter"/> instance.
        /// </summary>
        public static SampleConverter UnsignedEightBit { get => new UnsignedEightBitConverter(); }

        /// <summary>
        /// Gets a new <see cref="SampleConverter"/> instance.
        /// </summary>
        public static SampleConverter SignedSixteenBit { get => new SignedSixteenBitConverter(); }

        /// <summary>
        /// Gets a new <see cref="SampleConverter"/> instance.
        /// </summary>
        public static SampleConverter SignedThirtyTwoBit { get => new SignedThirtyTwoBitConverter(); }

        /// <summary>
        /// Gets the bit depth of the given <see cref="SampleConverter"/>.
        /// </summary>
        public abstract int BitDepth { get; }

        /// <summary>
        /// When overridden in a derived class, converts the sample into an array of bytes that represents the sample in the expected bit depth.
        /// </summary>
        /// <param name="sample">Any sample ranging from -1 to +1.</param>
        /// <returns>An array of bytes with size of <see cref="BitDepth"/> / 8.</returns>
        public abstract byte[] ConvertSample(double sample);
    }
}