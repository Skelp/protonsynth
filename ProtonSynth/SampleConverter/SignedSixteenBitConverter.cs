﻿using System;

namespace Skelp.ProtonSynth
{
    public class SignedSixteenBitConverter : SampleConverter
    {
        public override int BitDepth => 16;

        public override byte[] ConvertSample(double sample)
        {
            var sampleAsShort = (short)Math.Round(ValueHandler.ChangeRange(sample, -1, 1, short.MinValue, short.MaxValue));
            var sampleBytes = BitConverter.GetBytes(sampleAsShort);
            return sampleBytes;
        }
    }
}