﻿using System;

namespace Skelp.ProtonSynth
{
    public class SignedThirtyTwoBitConverter : SampleConverter
    {
        public override int BitDepth => 32;

        public override byte[] ConvertSample(double sample)
        {
            var sampleAsInt = (int)Math.Round(ValueHandler.ChangeRange(sample, -1, 1, int.MinValue, int.MaxValue));
            var sampleBytes = BitConverter.GetBytes(sampleAsInt);
            return sampleBytes;
        }
    }
}