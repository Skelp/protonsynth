﻿using System;

namespace Skelp.ProtonSynth
{
    public class UnsignedEightBitConverter : SampleConverter
    {
        public override int BitDepth => 8;

        public override byte[] ConvertSample(double sample)
        {
            var sampleAsByte = (byte)Math.Round(ValueHandler.ChangeRange(sample, -1, 1, byte.MinValue, byte.MaxValue));
            return new byte[] { sampleAsByte };
        }
    }
}