# Features

You will find documentation on the most important features of `ProtonSynth` here.

Parameter values will be assigned **explicitly** in every code sample for clarity. 

For instance, instead of `Console.WriteLine("Hello World")` you would find `Console.WriteLine(value: "Hello World")`, which is valid C# code in both cases.

## OverSamplingWrapper

`OverSamplingWrapper` makes handling oversampling easier by automating `samplerate` changes,
as well as encapsulating the logic of downsampling to the original `samplerate`.

It is highly recommended to oversample your IOscillator instances to avoid rough [aliasing](https://en.wikipedia.org/wiki/Aliasing#Online_audio_example).

You can wrap any `IOscillator` with this `class`.

Sample usage:
```csharp
// Predefined target samplerate
int sampleRate = 44100;

// Get Oscillator
IOscillator osc = new SineOscillator(sampleRate: sampleRate, frequency: 440, amplitudeMultiplier: 1);

// Oversampling with a factor of 4, which means it will interally oversample our IOscillator with a samplerate of 4 x 44100 = 176400
OverSamplingWrapper oscWrapper = new OverSamplingWrapper(oscillator: osc, sampleRate: sampleRate, overSamplingFactor: 4);

// Instead of calling osc.GetSample(), use the wrapper. It will return a sample in the original sampleRate as expected.
oscWrapper.GetSample();
```

## PitchBend

A `PitchBend` instance will calculate the `frequency` delta to the originally provided `frequency`. The frequency, semitone range as well as the pitchbend position can be adjusted.

```csharp
// Note given to the oscillator to play
double noteFrequency = 440;

var osc = new Oscillators.SineOscillator(sampleRate: 44100, frequency: noteFrequency);

// Semitone range of 2 in both up and down direction
PitchBend pitchBend = new PitchBend(frequency: noteFrequency, semitoneRange: 2);
// Bend position of 0.5 represents the middle of the pitchbend, no bending will be applied
pitchBend.BendPosition = 0.5;

// Should return 0, as there is effectively no bending applied
double frequencyDelta = pitchBend.GetFrequencyDifference();

// PitchBend instances carry the original frequency
// Now the final frequency can be given to the Oscillator
osc.Frequency = pitchBend.Frequency + frequencyDelta;

osc.GetSample();
```

## Signal generators

### NoiseGenerator

### Oscillators (IOscillator)

#### Oscillator

#### SineOscillator

#### FastSineOscillator

#### PulseOscillator

#### SawtoothOscillator

#### TriangleOscillator

#### WavetableOscillator

## Envelopes (IEnvelope)

### Envelope

### LinearEnvelope

### PowerEnvelope

## SignalProcessors (ISignalProcessor)

### Filter

## MidiFrequencyArray

## IVoice

An `IVoice` implementation represents the parameters and synthesis of one voice within a given synthesizer.

Down below you will find a simple implementation of an IVoice with a single `SineOscillator` and a `LinearEnvelope`.

```csharp

```

## PolyphonyManager
