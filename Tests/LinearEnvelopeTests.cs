using Microsoft.VisualStudio.TestTools.UnitTesting;
using Skelp.ProtonSynth.Envelopes;
using System;

namespace Skelp.ProtonSynth.Tests
{
    [TestClass]
    public class LinearEnvelopeTests
    {
        [DataTestMethod]
        [DataRow(1)]
        [DataRow(44100)]
        [DataRow(int.MaxValue)]
        public void GetAmplitude_IsDefaultAndOff_ReturnsZero(int sampleRate)
        {
            // Arrange
            var linearEnvelope = new LinearEnvelope(sampleRate);

            // Act
            var result = linearEnvelope.GetAmplitude();

            // Assert
            Assert.AreEqual(0, result);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(44100)]
        [DataRow(int.MaxValue)]
        public void GetAmplitude_IsDefaultAndOn_ReturnsSustainAmplitude(int sampleRate)
        {
            // Arrange
            var linearEnvelope = new LinearEnvelope(sampleRate);
            linearEnvelope.SustainAmplitude = 0.5;
            linearEnvelope.TriggerOn();

            // Act
            var result = linearEnvelope.GetAmplitude();

            // Assert
            Assert.AreEqual(linearEnvelope.SustainAmplitude, result);
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(0.5)]
        [DataRow(1)]
        public void GetAmplitude_SustainAtValueAndOn_ReturnsValue(double value)
        {
            // Arrange
            var linearEnvelope = new LinearEnvelope();
            linearEnvelope.SustainAmplitude = value;
            linearEnvelope.TriggerOn();

            // Act
            var result = linearEnvelope.GetAmplitude();

            // Assert
            Assert.AreEqual(value, (double)result);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(44100)]
        [DataRow(int.MaxValue)]
        public void SetSampleRate_ValidRange_SampleRateEqualsInput(int sampleRate)
        {
            // Arrange
            var linearEnvelope = new LinearEnvelope();

            // Act
            linearEnvelope.SetSampleRate(sampleRate);

            // Assert
            Assert.AreEqual(sampleRate, linearEnvelope.SampleRate);
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(-1)]
        [DataRow(int.MinValue)]
        public void SetSampleRate_InvalidRange_ThrowsException(int sampleRate)
        {
            // Arrange
            var linearEnvelope = new LinearEnvelope();

            // Act
            Action act = () => linearEnvelope.SetSampleRate(sampleRate);

            // Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(act);
        }

        [DataTestMethod]
        [DataRow(2, 0.5)]
        [DataRow(44100, 0.5)]
        [DataRow(int.MaxValue, 0.5)]
        public void TriggerOn_GetAmplitude_ReturnsZero(int sampleRate, double attackSeconds)
        {
            // Arrange
            var linearEnvelope = new LinearEnvelope(sampleRate);
            linearEnvelope.AttackTime = TimeSpan.FromSeconds(attackSeconds);

            // Act
            linearEnvelope.TriggerOn();
            var result = linearEnvelope.GetAmplitude();

            // Assert
            Assert.AreEqual(0, (double)result);
        }
        
        [DataTestMethod]
        [DataRow(2, 0, 0)]
        [DataRow(2, 1, 0.5)]
        [DataRow(4, 1, 0.75)]
        public void TriggerOff_GetAmplitude_ReturnsExpected(int sampleRate, double releaseSeconds, double expected)
        {
            // Arrange
            var linearEnvelope = new LinearEnvelope(sampleRate);
            linearEnvelope.ReleaseTime = TimeSpan.FromSeconds(releaseSeconds);
            linearEnvelope.TriggerOn();
            linearEnvelope.GetAmplitude();

            // Act
            linearEnvelope.TriggerOff();
            var result = linearEnvelope.GetAmplitude();

            // Assert
            Assert.AreEqual(expected, (double)result);
        }
    }
}