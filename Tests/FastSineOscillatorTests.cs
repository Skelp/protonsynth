﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Skelp.ProtonSynth.SignalGenerators;

namespace Skelp.ProtonSynth.Tests
{
    [TestClass]
    public class FastSineOscillatorTests
    {
        [DataTestMethod]
        [DataRow(4)]
        [DataRow(8)]
        public void GetSample_Peak_ApproximatesOne(int sampleRate)
        {
            // Arrange
            var osc = new FastSineOscillator(sampleRate, 1);

            for (int i = 0; i < sampleRate / 4; i++)
            {
                osc.GetSample();
            }

            // Act
            var result = osc.GetSample();

            // Assert
            Assert.AreEqual(1, result, 0.001);
        }

        [DataTestMethod]
        [DataRow(4)]
        [DataRow(8)]
        public void GetSample_Trough_ApproximatesNegativeOne(int sampleRate)
        {
            // Arrange
            var osc = new FastSineOscillator(sampleRate, 1);

            for (int i = 0; i < sampleRate / 4 * 3; i++)
            {
                osc.GetSample();
            }

            // Act
            var result = osc.GetSample();

            // Assert
            Assert.AreEqual(-1, result, 0.001);
        }
    }
}