﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Skelp.ProtonSynth.Tests
{
    [TestClass]
    public class WaveBuilderTests
    {
        // Size of the RIFF WAVE file header in bytes
        private const int HeaderLength = 44;

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(1)]
        [DataRow(4)]
        public void GetWave_VaryingSamples8Bit_ValidLength(int sampleCount)
        {
            // Arrange
            var wb = new WaveBuilder(44100, SampleConverter.UnsignedEightBit);
            wb.AddSamples(new double[sampleCount]);

            // Act
            var result = wb.ToByteArray();

            // Assert
            Assert.AreEqual(HeaderLength + sampleCount, result.Length);
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(1)]
        [DataRow(4)]
        public void GetWave_VaryingSamples16Bit_ValidLength(int sampleCount)
        {
            // Arrange
            var wb = new WaveBuilder(44100, SampleConverter.SignedSixteenBit);
            wb.AddSamples(new double[sampleCount]);

            // Act
            var result = wb.ToByteArray();

            // Assert
            Assert.AreEqual(HeaderLength + sampleCount * 2, result.Length);
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(1)]
        [DataRow(4)]
        public void GetWave_VaryingSamples32Bit_ValidLength(int sampleCount)
        {
            // Arrange
            var wb = new WaveBuilder(44100, SampleConverter.SignedThirtyTwoBit);
            wb.AddSamples(new double[sampleCount]);

            // Act
            var result = wb.ToByteArray();

            // Assert
            Assert.AreEqual(HeaderLength + sampleCount * 4, result.Length);
        }
    }
}