﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Skelp.ProtonSynth.SignalProcessors;
using System;

namespace Skelp.ProtonSynth.Tests
{
    [TestClass]
    public class FeedbackDelayTests
    {
        [DataTestMethod]
        [DataRow(2, 1, 1)]
        [DataRow(2, 1, -1)]
        [DataRow(2, 1, 0)]
        public void ProcessSample_SingleDelayMaxAmplitude_ReturnsValue(int sampleRate, double delaySize, double value)
        {
            // Arrange
            var delay = new FeedbackDelay(sampleRate);
            delay.MaxAmplitude = 1;
            delay.Feedback = 0;
            delay.Resize(TimeSpan.FromSeconds(delaySize));
            delay.ProcessSample(value);

            // Act
            var result = delay.ProcessSample(0);

            // Assert
            Assert.AreEqual(value, (double)result);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(44100)]
        [DataRow(int.MaxValue)]
        public void SetSampleRate_ValidRange_SampleRateEqualsInput(int sampleRate)
        {
            // Arrange
            var delay = new FeedbackDelay(1);

            // Act
            delay.SetSampleRate(sampleRate);

            // Assert
            Assert.AreEqual(sampleRate, delay.SampleRate);
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(-1)]
        [DataRow(int.MinValue)]
        public void SetSampleRate_InvalidRange_ThrowsException(int sampleRate)
        {
            // Arrange
            var delay = new FeedbackDelay(1);

            // Act
            Action act = () => delay.SetSampleRate(sampleRate);

            // Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(act);
        }
    }
}