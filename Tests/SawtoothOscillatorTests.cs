﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Skelp.ProtonSynth.SignalGenerators;

namespace Skelp.ProtonSynth.Tests
{
    [TestClass]
    public class SawtoothOscillatorTests
    {
        [DataTestMethod]
        [DataRow(4)]
        [DataRow(8)]
        public void GetSample_Peak_ReturnsHalf(int sampleRate)
        {
            // Arrange
            var osc = new SawtoothOscillator(sampleRate, 1);

            for (int i = 0; i < sampleRate / 4; i++)
            {
                osc.GetSample();
            }

            // Act
            var result = osc.GetSample();

            // Assert
            Assert.AreEqual(0.5, result);
        }

        [DataTestMethod]
        [DataRow(4)]
        [DataRow(8)]
        public void GetSample_Trough_ReturnsNegativeHalf(int sampleRate)
        {
            // Arrange
            var osc = new SawtoothOscillator(sampleRate, 1);

            for (int i = 0; i < sampleRate / 4 * 3; i++)
            {
                osc.GetSample();
            }

            // Act
            var result = osc.GetSample();

            // Assert
            Assert.AreEqual(-0.5, result);
        }
    }
}