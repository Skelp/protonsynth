﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Skelp.ProtonSynth.SignalGenerators;

namespace Skelp.ProtonSynth.Tests
{
    [TestClass]
    public class SineOscillatorTests
    {
        [DataTestMethod]
        [DataRow(4)]
        [DataRow(8)]
        public void GetSample_Peak_ReturnsOne(int sampleRate)
        {
            // Arrange
            var osc = new SineOscillator(sampleRate, 1);

            for (int i = 0; i < sampleRate / 4; i++)
            {
                osc.GetSample();
            }

            // Act
            var result = osc.GetSample();

            // Assert
            Assert.AreEqual(1, result);
        }

        [DataTestMethod]
        [DataRow(4)]
        [DataRow(8)]
        public void GetSample_Trough_ReturnsNegativeOne(int sampleRate)
        {
            // Arrange
            var osc = new SineOscillator(sampleRate, 1);

            for (int i = 0; i < sampleRate / 4 * 3; i++)
            {
                osc.GetSample();
            }

            // Act
            var result = osc.GetSample();

            // Assert
            Assert.AreEqual(-1, result);
        }
    }
}