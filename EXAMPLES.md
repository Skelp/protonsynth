# Examples

## Table Of Contents

- [Mixing Two Or More Signal Generators](#mixing-two-or-more-signal-generators)
- [Using A Filter](#using-a-filter)
    - [Using An LFO On Filter Frequency](#using-an-lfo-on-filter-frequency)
- [Oversampling](#oversampling)
- [Pitchbending](#pitchbending)
- [Writing A WAVE File](#writing-a-wave-file)
- [Using The PolyphonyManager](#using-the-polyphonymanager)
- [Putting It All Together](#putting-it-all-together)

## Mixing Two Or More Signal Generators

Combining the output of two (or more) `ISignalGenerator` instances is simple.  
Gather the samples of each instance, add them to a single value and divide by the amount of instances to stay within the range of -1 and +1.  
This method ensures a distortion-free output at any given moment.

Starting from version `0.5.0`, this method is implemented in the class `Skelp.ProtonSynth.SignalGenerators.SignalMixer`. Below you will find an example on how to use it. 

```csharp
int sampleRate = 44100;
double frequency = 440;
double amplitude = 1;

TriangleOscillator tri = new TriangleOscillator(sampleRate, frequency, amplitude);
SawtoothOscillator saw = new SawtoothOscillator(sampleRate, frequeency, amplitude);

SignalMixer mixer = new SignalMixer(tri, saw);

// Retrieves the next sample
mixer.GetSample();
```

## Using A Filter



### Using An LFO On Filter Frequency

## Oversampling

Digital synthesizers work in the realm of [discrete time](https://en.wikipedia.org/wiki/Discrete_time_and_continuous_time), meaning they are sampling in finite steps.  
Due to this fact it is very likely that an oscillator or audio effect produces [audible aliasing artifacts](https://en.wikipedia.org/wiki/Aliasing#Sampling_sinusoidal_functions).

To reduce this aliasing you may consider oversampling your processing.  
This is achieved by processing audio at a higher samplerate than your desired output samplerate.

The `OverSamplingWrapper` class implements a rudimentary oversampling for `IOscillator` classes.  
Down below you will find an example of a `SineOscillator` oversampled by a factor of 4.  
The example will demonstrate how to operate the wrapper and get samples out of it.

```csharp
int sampleRate = 44100;
double frequency = 440;
double amplitude = 1;

SineOscillator osc = new SineOscillator(sampleRate, frequency,  amplitude);

int overSamplingFactor = 4;

// Oversampling with a factor of 4, which means it will interally oversample our IOscillator with a samplerate of 4 x 44100 = 176400
OverSamplingWrapper oscWrapper = new OverSamplingWrapper(osc, sampleRate, overSamplingFactor);

// Instead of calling osc.GetSample(), use the wrapper. It will return a sample in the original sampleRate as expected.
oscWrapper.GetSample();
```

## Pitchbending



```csharp
// Note given to the oscillator to play
double noteFrequency = 440;

// Represents a midi-note range of -2 to +2 semitones
int semitoneRange = 2;

var osc = new Oscillators.SineOscillator(sampleRate: 44100, frequency: noteFrequency);

PitchBend pitchBend = new PitchBend(noteFrequency, semitoneRange);

// Bend position of 0.5 represents the middle of the pitchbend, no bending will be applied
pitchBend.BendPosition = 0.5;

// Should return 0 with a BendPosition of 0.5, as there is effectively no bending applied
double frequencyDelta = pitchBend.GetFrequencyDifference();

// PitchBend instances carry the original frequency
// Now the final frequency can be given to the Oscillator
osc.Frequency = pitchBend.Frequency + frequencyDelta;

osc.GetSample();
```

## Writing A WAVE File

`ProtonSynth >= 0.6.0`

ProtonSynth provides the `Skelp.ProtonSynth.WaveBuilder` class to easily construct RIFF WAVE files from samples.  
Down below is an example for `.NET 5.0` in which a `SineOscillator` is generating one second worth of samples and writes it into a `WaveBuilder` instance. The output is then returned by calling `WaveBuilder.ToByteArray()` and then saved using `System.IO.File.WriteAllBytes`.

The result is a mono, signed 16-bit RIFF WAVE file with a sample rate of 44.1kHz. 

```csharp
const int sampleRate = 44100;
var osc = new SineOscillator(sampleRate);
var wb = new WaveBuilder(sampleRate, SampleConverter.SignedSixteenBit);

for (int i = 0; i < sampleRate; i++)
{
    wb.AddSample(osc.GetSample());
}

byte[] result = wb.ToByteArray();

File.WriteAllBytes("out.wav", result);
```

## Using The PolyphonyManager
